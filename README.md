#mygit

### APP介绍：
mygit是根据开源中国码云Git@OSC的API所做的代码托管的APP，融合了目前较为流行的设计元素，项目的UI设计采用 Google Material Design 设计风格，项目框架采用MVP模式，代码结构清晰，容易扩展，网络请求采用了时下流行的RxJava + Retrofit组合。

### 功能说明：

<发现>，按类型分为：推荐项目、热门项目、最近更新

<语言>，按类型分为：java、android、php等

<消息>，按类型分为：未读消息、已读消息

<我>，用户信息，有最近动态、个人项目、start项目、watch项目

查看项目详情、readme、问题、提交、代码库、代码详情
 
登陆、注销登陆、查看个人信息

### 开源技术：

1. [Rxjava](https://github.com/ReactiveX/RxJava)
1. [RxAndroid](https://github.com/ReactiveX/RxAndroid)
1. [Retrofit](https://github.com/square/retrofit)
1. [Glide](https://github.com/bumptech/glide)
1. [Butter Knife](https://github.com/JakeWharton/butterknife)
1. [okhttp3](https://github.com/square/okhttp)
1. [Elegant](https://git.oschina.net/LiuJinChong/Elegant)
1. [AndroidBase](https://git.oschina.net/LiuJinChong/AndroidBase)

### 效果预览：


![输入图片说明](https://git.oschina.net/uploads/images/2017/0501/232806_e004d772_1022857.png "在这里输入图片标题")![输入图片说明](https://git.oschina.net/uploads/images/2017/0501/232821_af1a6f30_1022857.png "在这里输入图片标题")![输入图片说明](https://git.oschina.net/uploads/images/2017/0501/232945_09b24953_1022857.png "在这里输入图片标题")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0501/233443_2a8a2fa2_1022857.png "在这里输入图片标题")![输入图片说明](https://git.oschina.net/uploads/images/2017/0501/233109_230de447_1022857.png "在这里输入图片标题")![输入图片说明](https://git.oschina.net/uploads/images/2017/0501/233409_619d3382_1022857.png "在这里输入图片标题")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0501/233526_917beabd_1022857.png "在这里输入图片标题")![输入图片说明](https://git.oschina.net/uploads/images/2017/0501/233558_8f384a65_1022857.png "在这里输入图片标题")![输入图片说明](https://git.oschina.net/uploads/images/2017/0501/233705_798c5465_1022857.png "在这里输入图片标题")