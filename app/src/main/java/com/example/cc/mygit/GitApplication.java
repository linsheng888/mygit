package com.example.cc.mygit;

import android.app.Application;

import com.example.cc.mygit.source.GitAppConfig;
import com.example.cc.mygit.utils.CacheManager;
import com.example.cc.mygit.utils.ImageLoader;

/**
 * Created by cc on 2016/11/12.
 */

public class GitApplication extends Application{
    private static GitAppConfig mConfig;

    @Override
    public void onCreate() {
        super.onCreate();
        mConfig = new GitAppConfig(this,"git_data");
        CacheManager.init(getApplicationContext());
        ImageLoader.init(getApplicationContext());
    }

    public static GitAppConfig getConfig(){
        return mConfig;
    }
}
