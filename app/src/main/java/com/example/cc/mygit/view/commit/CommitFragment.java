package com.example.cc.mygit.view.commit;

import android.os.Bundle;

import com.example.cc.mygit.base.BaseRecyclerFragment;
import com.example.cc.mygit.model.Commit;
import com.example.cc.mygit.model.Project;
import com.haibin.androidbase.adapter.BaseRecyclerAdapter;

/**
 * Created by cc on 2017/1/1.
 */

public class CommitFragment extends BaseRecyclerFragment<CommitContract.Presenter, Commit>
        implements CommitContract.View {

    public static CommitFragment newInstance(Project project) {
        CommitFragment fragment = new CommitFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("project", project);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected BaseRecyclerAdapter<Commit> getAdapter() {
        return new CommitAdapter(mContext);
    }

    @Override
    public void onItemClick(int position, long itemId) {

    }
}
