package com.example.cc.mygit.model;

/**
 * Created by cc on 2016/12/13.
 */

public class Count extends BaseModel {
    private int count;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
