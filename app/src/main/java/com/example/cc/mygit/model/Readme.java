package com.example.cc.mygit.model;

/**
 * Created by cc on 2017/1/1.
 */

public class Readme extends BaseModel{
    private String content;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
