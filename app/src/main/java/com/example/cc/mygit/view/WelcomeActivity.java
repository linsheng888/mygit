package com.example.cc.mygit.view;

import android.os.Handler;

import com.example.cc.mygit.GitApplication;
import com.example.cc.mygit.R;
import com.example.cc.mygit.view.login.LoginActivity;
import com.haibin.androidbase.activity.BaseActivity;
import com.haibin.httpnet.HttpNetClient;
import com.haibin.httpnet.builder.Request;
import com.haibin.httpnet.core.Response;
import com.haibin.httpnet.core.call.CallBack;

/**
 * Created by cc on 2016/11/12.
 */

public class WelcomeActivity extends BaseActivity {
    @Override
    protected int getLayoutId() {
        return R.layout.activity_welcome;
    }

    @Override
    protected void initView() {

//        new HttpNetClient()
//                .newCall(new Request.Builder()
//                        .url("https://106.14.35.15:3001/mobile/login")
//                        .method("POST")
//                        .build())
//                .execute(new CallBack() {
//                    @Override
//                    public void onResponse(Response response) {
//                        if (response != null) {
//
//                        }
//                    }
//
//                    @Override
//                    public void onFailure(Exception e) {
//                        if (e != null) {
//
//                        }
//                    }
//                });

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (GitApplication.getConfig().getUser() == null){
                    LoginActivity.show(WelcomeActivity.this);
                }else{
                    MainActivity.show(WelcomeActivity.this);
                }
                finish();
            }
        },1500);
    }

    @Override
    public void onBackPressed() {

    }
}

