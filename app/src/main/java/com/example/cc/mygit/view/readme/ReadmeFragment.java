package com.example.cc.mygit.view.readme;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.cc.mygit.R;
import com.example.cc.mygit.base.BaseWebFragment;
import com.example.cc.mygit.model.Project;
import com.example.cc.mygit.model.Readme;
import com.example.cc.mygit.model.User;
import com.example.cc.mygit.utils.ImageLoader;
import com.example.cc.mygit.view.userInfo.UserInfoActivity;
import com.haibin.androidbase.utils.DateTimeFormat;
import com.haibin.androidbase.widget.CircleImageView;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by cc on 2017/1/1.
 */

public class ReadmeFragment extends BaseWebFragment implements
        ReadmeContract.View, View.OnClickListener{

    @Bind(R.id.tv_project_name)
    TextView tv_project_name;

    @Bind(R.id.tv_time)
    TextView tv_time;

    @Bind(R.id.tv_title)
    TextView tv_title;

    @Bind(R.id.civ_author)
    CircleImageView mImageAuthor;

    private Project mProject;
    ReadmePresenter presenter;

    public static ReadmeFragment newInstance(Project project) {
        ReadmeFragment fragment = new ReadmeFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("project", project);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_readme;
    }

    @Override
    protected void initData() {
        ButterKnife.bind(this,mRootView);
        super.initData();
        mProject = (Project)getArguments().getSerializable("project");
        tv_project_name.setText(mProject.getOwner().getName());
        tv_time.setText(DateTimeFormat.format(mProject.getCreatedTime()));
        tv_title.setText(mProject.getDescription());
        ImageLoader.getGlide()
                .load(mProject.getOwner().getNewPortrait())
                .asBitmap()
                .placeholder(R.mipmap.icon_git)
                .into(mImageAuthor);
    }
    @OnClick({R.id.btn_watch,R.id.civ_author})
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_readme:
                ReadmeActivity.show(mContext, mProject);
                break;

            case R.id.civ_author:
                UserInfoActivity.show(mContext,mProject);
                break;
        }
    }

    @Override
    public void showGetReadmeSuccess(Readme readme) {
        mWebView.loadDataWithBaseURL("file:///android_asset/", readme.getContent(), "text/html", "htf-8", null);
    }

    @Override
    public void showGetReadmeError(int strId) {

    }

    @Override
    public void showNetworkError(int strId) {

    }

    @Override
    public void setPresenter(ReadmeContract.Presenter presenter) {

    }
}
