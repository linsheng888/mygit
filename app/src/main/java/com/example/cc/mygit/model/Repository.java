package com.example.cc.mygit.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by cc on 2017/5/1.
 */

public class Repository implements Serializable {

    @SerializedName("name")
    private String _name;

    @SerializedName("url")
    private String _url;

    @SerializedName("description")
    private String _description;

    @SerializedName("homePage")
    private String _homePage;

    public String getName() {
        return _name;
    }
    public void setName(String name) {
        this._name = name;
    }
    public String getUrl() {
        return _url;
    }
    public void setUrl(String url) {
        this._url = url;
    }
    public String getDescription() {
        return _description;
    }
    public void setDescription(String description) {
        this._description = description;
    }
    public String getHomePage() {
        return _homePage;
    }
    public void setHomePage(String homePage) {
        this._homePage = homePage;
    }
}
