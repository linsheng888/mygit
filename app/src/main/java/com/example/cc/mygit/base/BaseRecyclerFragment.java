package com.example.cc.mygit.base;

import com.example.cc.mygit.model.Project;
import com.example.cc.mygit.present.BaseListPresenter;
import com.example.cc.mygit.present.BaseListView;
import com.haibin.androidbase.adapter.BaseRecyclerAdapter;
import com.haibin.androidbase.fragment.BaseRecyclerViewFragment;
import com.haibin.androidbase.utils.T;

import java.util.List;

/**
 * Created by cc on 2016/11/12.
 */

public abstract class BaseRecyclerFragment<P extends BaseListPresenter, M>
        extends BaseRecyclerViewFragment<M> implements BaseListView<P, M> {

    protected BaseListPresenter mPresenter;

    @Override
    public void onRefreshing() {
        mPresenter.onRefreshing();
    }

    @Override
    public void onLoadMore() {
        mAdapter.setFooterState(BaseRecyclerAdapter.FOOTER_STATE_LOADING, true);
        mPresenter.onLoadMore();
    }

    @Override
    public void onRefreshSuccess(List<M> items) {
        mAdapter.resetItem(items);
    }

    @Override
    public void onLoadMoreSuccess(List<M> items) {
        mAdapter.addAll(items);
    }

    @Override
    public void showNetWorkError() {
        mAdapter.setFooterState(BaseRecyclerAdapter.FOOTER_STATE_INVALID_NETWORK, true);
    }

    @Override
    public void onComplete() {
        mSuperRefreshLayout.onComplete();
    }

    @Override
    public void onRefreshFailure() {
        // TODO: 2016/11/7 下拉刷新失败
    }

    @Override
    public void showNoMoreData() {
        // TODO: 2016/11/7 加载更多没有数据
        mAdapter.setFooterState(BaseRecyclerAdapter.FOOTER_STATE_NO_MORE, true);
        mSuperRefreshLayout.setNoMoreData();
    }
    // cc: 2017/03/19 弹出--加载更多没有数据
    public void showToastShort(String string) {
        T.showToastShort(getContext(), string);
    }

    @Override
    public void setPresenter(P presenter) {
        this.mPresenter = presenter;
    }
}
