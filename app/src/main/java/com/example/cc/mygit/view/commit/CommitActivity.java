package com.example.cc.mygit.view.commit;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.view.Menu;
import android.view.MenuItem;

import com.example.cc.mygit.R;
import com.example.cc.mygit.model.Project;
import com.example.cc.mygit.view.branch.BranchDialog;
import com.haibin.androidbase.activity.BaseBackActivity;

/**
 * Created by cc on 2017/1/1.
 */

public class CommitActivity extends BaseBackActivity {

    private CommitPresenter mPresenter;
    private Project mProject;
    private BottomSheetBehavior mBehavior;
    private BranchDialog mDialog;

    public static void show(Context context, Project project) {
        Intent intent = new Intent(context, CommitActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("project", project);
        intent.putExtras(bundle);
        context.startActivity(intent);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_commit;
    }

    @Override
    protected void initData() {
        super.initData();
        mProject = (Project) getIntent().getExtras().getSerializable("project");
        CommitFragment fragment = CommitFragment.newInstance(mProject);
        addFragment(R.id.fl_content, fragment);
        mDialog = new BranchDialog(this, mProject);
        mPresenter = new CommitPresenter(fragment, mProject);
        mPresenter.start();
        mBehavior = BottomSheetBehavior.from(findViewById(R.id.fl_bottom_sheet));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_branch, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_branch) {
            if (mDialog.isShowing()) {
                mDialog.dismiss();
            } else {
                mDialog.show();
            }
        }
        return super.onOptionsItemSelected(item);
    }
}


