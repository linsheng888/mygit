package com.example.cc.mygit.utils;

import android.text.TextUtils;

import static java.util.Locale.US;

/**
 * Created by cc on 2017/2/24.
 */

public class MarkdownUtils {
    private static final String[] MARKDOWN_EXTENSIONS = { ".md", ".mkdn",
            ".mdwn", ".mdown", ".markdown", ".mkd", ".mkdown", ".ron" };

    /**
     * Is the the given file name a Markdown file?
     *
     * @param name
     * @return true if the name has a markdown extension, false otherwise
     */
    public static boolean isMarkdown(String name) {
        if (TextUtils.isEmpty(name))
            return false;

        name = name.toLowerCase(US);
        for (String extension : MARKDOWN_EXTENSIONS)
            if (name.endsWith(extension))
                return true;

        return false;
    }
}
