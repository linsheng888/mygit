package com.example.cc.mygit.view.code;

import com.example.cc.mygit.model.Code;
import com.example.cc.mygit.present.BaseListPresenter;
import com.example.cc.mygit.present.BaseListView;

import java.util.List;

/**
 * Created by cc
 * on 2017/1/25.
 */

public interface CodeContract {

    interface ActionView {
        void onAddPath(String path);
    }

    interface View extends BaseListView<Presenter, Code> {
        void onPreLoadSuccess(List<Code> codes);

    }

    interface Presenter extends BaseListPresenter {
        void load(String path);

        void preLoad(int position);

        //移除代码
        void remove(int position);
    }
}
