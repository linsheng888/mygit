package com.example.cc.mygit.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by cc on 2017/2/22.
 */

public class Branch extends BaseModel {

    private String name;
    private CommitMessage commit;

    @SerializedName("protected")
    private Boolean isProtected;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CommitMessage getCommit() {
        return commit;
    }

    public void setCommit(CommitMessage commit) {
        this.commit = commit;
    }

    public Boolean getProtected() {
        return isProtected;
    }

    public void setProtected(Boolean aProtected) {
        isProtected = aProtected;
    }


}
