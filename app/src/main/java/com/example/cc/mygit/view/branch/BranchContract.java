package com.example.cc.mygit.view.branch;

import com.example.cc.mygit.model.Branch;
import com.example.cc.mygit.present.BaseListPresenter;
import com.example.cc.mygit.present.BaseListView;

/**
 * Created by cc on 2017/2/22.
 */

public interface BranchContract {

    interface View extends BaseListView<Presenter,Branch>{

    }

    interface Presenter extends BaseListPresenter{

    }

}
