package com.example.cc.mygit.view.login;

import com.example.cc.mygit.model.User;
import com.haibin.elegant.call.Call;
import com.haibin.elegant.net.File;
import com.haibin.elegant.net.Form;
import com.haibin.elegant.net.Headers;
import com.haibin.elegant.net.POST;

/**
 * Created by cc on 2016/11/13.
 */

public interface LoginService {

    @POST("session")
    Call<User> login(@Form("email") String email,
                     @Form("password") String pwd);
}


