package com.example.cc.mygit.view.language;

import com.example.cc.mygit.model.Language;
import com.example.cc.mygit.source.API;
import com.haibin.elegant.Response;
import com.haibin.elegant.call.CallBack;

import java.util.List;

/**
 * Created by cc on 2016/12/7.
 */

public class LanguagePresenter implements LanguageContract.Presenter{

    private final LanguageContract.View mView;

    public LanguagePresenter(LanguageContract.View mView) {
        this.mView = mView;
        this.mView.setPresenter(this);
    }

    @Override
    public void onRefreshing() {
        API.getLanguages(new CallBack<List<Language>>() {
            @Override
            public void onResponse(Response<List<Language>> response) {
                List<Language> items = response.getBody();
                if (items != null){
                    mView.onRefreshSuccess(items);
                }else{
                    mView.onRefreshFailure();
                }
                mView.onComplete();
            }

            @Override
            public void onFailure(Exception e) {
                mView.showNetWorkError();
                mView.onComplete();

            }
        });

    }

    @Override
    public void onLoadMore() {

    }

    @Override
    public void start() {

    }
}
