package com.example.cc.mygit.view.userInfo.userProjects;

import android.os.Bundle;

import com.example.cc.mygit.base.BaseRecyclerFragment;

import com.example.cc.mygit.model.Project;
import com.example.cc.mygit.model.User;

import com.example.cc.mygit.view.detail.ProjectDetailActivity;
import com.example.cc.mygit.view.project.ProjectAdapter;
import com.example.cc.mygit.view.project.ProjectContract;
import com.example.cc.mygit.view.userInfo.userStartProjects.UserStartProjectsPresenter;
import com.example.cc.mygit.view.userInfo.userWatchProjects.UserWatchProjectsPresenter;
import com.haibin.androidbase.adapter.BaseRecyclerAdapter;


/**
 * Created by cc
 * on 2017/3/18.
 */

public class UserProjectsFragment extends BaseRecyclerFragment<ProjectContract.Presenter, Project> implements ProjectContract.View {

    private Project mProject;
    private User mUser;
    private String mType;

    public static UserProjectsFragment newInstance(Project project, String type) {
        UserProjectsFragment userProjectsFragment = new UserProjectsFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("project", project);
        bundle.putString("type", type);
        userProjectsFragment.setArguments(bundle);
        return userProjectsFragment;
    }

    public static UserProjectsFragment newInstance(User user, String type) {
        UserProjectsFragment userProjectsFragment = new UserProjectsFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("user", user);
        bundle.putString("type", type);
        userProjectsFragment.setArguments(bundle);
        return userProjectsFragment;
    }

    @Override
    protected void initData() {
        mProject = (Project) getArguments().getSerializable("project");
        if (mProject != null) {
            mUser = mProject.getOwner();
        } else {
            mUser = (User) getArguments().getSerializable("user");
        }
        mType = getArguments().getString("type");
//        if(mType == "userProjects"){
//            new UserProjectsPresenter(this, mUser);
//        }else if (mType == "start"){
//new UserStartProjectsPresenter(this,mUser);
//        }else {
//            new UserWatchProjectsPresenter(this,mUser);
//        }
        switch (mType) {
            case "userProjects":
                new UserProjectsPresenter(this, mUser);
                break;
            case "start":
                new UserStartProjectsPresenter(this, mUser);
                break;
            case "watch":
                new UserWatchProjectsPresenter(this, mUser);
                break;
        }

        super.initData();
    }

    @Override
    protected void initView() {
        super.initView();

    }

    @Override
    protected BaseRecyclerAdapter<Project> getAdapter() {
        return new ProjectAdapter(getActivity());
    }

    @Override
    public void onItemClick(int position, long itemId) {
        ProjectDetailActivity.show(this, mAdapter.getItem(position), 1);
    }
}
