package com.example.cc.mygit.view.project;

import android.os.Bundle;

import com.example.cc.mygit.base.BaseRecyclerFragment;
import com.example.cc.mygit.model.Project;
import com.example.cc.mygit.view.detail.ProjectDetailActivity;
import com.haibin.androidbase.adapter.BaseRecyclerAdapter;

/**
 * Created by cc on 2016/11/13.
 */

public class ProjectFragment extends BaseRecyclerFragment<ProjectContract.Presenter, Project> implements ProjectContract.View {

    public static ProjectFragment newInstance(String type) {
        ProjectFragment fragment = new ProjectFragment();
        Bundle bundle = new Bundle();
        bundle.putString("type", type);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected void initData() {
        new ProjectPresenter(this, getArguments().getString("type")).start();
        super.initData();
    }

    @Override
    public void onItemClick(int position, long itemId) {
        ProjectDetailActivity.show(this,mAdapter.getItem(position),1);
    }

    @Override
    protected BaseRecyclerAdapter<Project> getAdapter() {
       return new ProjectAdapter(getActivity());
    }
}
