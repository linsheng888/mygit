package com.example.cc.mygit.view.code;

import android.content.Context;

import com.example.cc.mygit.R;
import com.haibin.androidbase.adapter.BaseRecyclerAdapter;
import com.haibin.androidbase.adapter.ViewHolder;

/**
 * Created by cc on 2017/2/24.
 */

class PathAdapter extends BaseRecyclerAdapter<String> {

    public PathAdapter(Context context) {
        super(context, NEITHER);
    }

    @Override
    protected int getLayoutId(int viewType) {
        return R.layout.item_list_path;
    }

    @Override
    protected void onBindDefaultViewHolder(ViewHolder holder, String item, int position) {

        holder.bindText(R.id.tv_path, item);
    }

    public void removeAll(int position) {
        if (position < 0 ||position>=mItems.size())
            return;
        int size = mItems.size();
        for (int i = position; i<=size;i++){
            int p = position +1;
            if (mItems.size()>p && p>=0)
                mItems.remove(p);
        }
        notifyDataSetChanged();

    }

}
