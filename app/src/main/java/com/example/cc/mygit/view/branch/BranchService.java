package com.example.cc.mygit.view.branch;

import com.example.cc.mygit.model.Branch;
import com.haibin.elegant.call.Call;
import com.haibin.elegant.net.Form;
import com.haibin.elegant.net.GET;
import com.haibin.elegant.net.Path;

import java.util.List;

/**
 * 分支模块
 * Created by cc on 2017/2/22.
 */

public interface BranchService {

    /**
     * 获取项目仓库分支
     */
    @GET("projects/{project_id}/repository/branches")
    Call<List<Branch>> getBranches(@Path("project_id") long project_id,
                                   @Form("private_token") String token);

    /**
     * 获取项目仓库标签
     *
     * @param project_owner_username 项目作者的username
     * @param project_name           项目的name
     */
    @GET("projects/{project_owner_username}/{project_name}/repository/branches")
    Call<List<Branch>> getTags(@Path("project_owner_username") String project_owner_username,
                               @Path("project_name") String project_name,
                               @Form("private_token") String token);

}
