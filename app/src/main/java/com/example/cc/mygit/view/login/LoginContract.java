package com.example.cc.mygit.view.login;

import com.example.cc.mygit.model.User;
import com.example.cc.mygit.present.BasePresenter;
import com.example.cc.mygit.present.BaseView;

/**
 * Created by cc on 2016/11/13.
 */

public interface LoginContract {

    interface View extends BaseView<Presenter>{

       void showLoginSuccess(int strId, User user);

        void showLoginError(int strId);

        void showNetError(int strId);

        void showEmailError(int strId);

        void showPasswordEmptyError(int strId);
    }

    interface Presenter extends BasePresenter{

        void login(String email, String pwd);

        void register();

        void rememberPassword();
    }
}
