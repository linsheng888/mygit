package com.example.cc.mygit.base;

import com.example.cc.mygit.R;
import com.example.cc.mygit.present.BaseListPresenter;
import com.example.cc.mygit.present.BaseListView;
import com.haibin.androidbase.activity.BaseRecyclerViewActivity;
import com.haibin.androidbase.adapter.BaseRecyclerAdapter;

import java.util.List;

/**
 * Created by cc on 2016/11/12.
 */

public abstract class BaseRecyclerActivity<P extends BaseListPresenter, M>
        extends BaseRecyclerViewActivity<M> implements BaseListView<P, M> {

    private P mPresenter;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_base_recycler;
    }

    @Override
    public void onRefreshing() {
        super.onRefreshing();
        mPresenter.onRefreshing();
    }

    @Override
    public void onLoadMore() {
        mAdapter.setFooterState(BaseRecyclerAdapter.FOOTER_STATE_LOADING,true);
        mPresenter.onLoadMore();
    }

    @Override
    public void onRefreshSuccess(List<M> items) {

        mAdapter.resetItem(items);
    }

    @Override
    public void onLoadMoreSuccess(List<M> items) {

        mAdapter.addAll(items);
    }

    @Override
    public void showNetWorkError() {

        mAdapter.setFooterState(BaseRecyclerAdapter.FOOTER_STATE_INVALID_NETWORK,true);
    }

    @Override
    public void onComplete() {

        mIsRefresh = false;
        mSuperRefreshLayout.onComplete();
    }

    @Override
    public void onRefreshFailure() {
        // TODO: 2016/11/7 下拉刷新失败
    }

    @Override
    public void showNoMoreData() {
        // TODO: 2016/11/7 加载更多没有数据
        mAdapter.setFooterState(BaseRecyclerAdapter.FOOTER_STATE_NO_MORE,true);
    }

    @Override
    public void setPresenter(P presenter) {
        this.mPresenter = presenter;
    }
}
