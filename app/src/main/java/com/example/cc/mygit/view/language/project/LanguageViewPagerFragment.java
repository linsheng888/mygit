package com.example.cc.mygit.view.language.project;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.example.cc.mygit.R;
import com.example.cc.mygit.base.BaseViewPagerFragment;
import com.example.cc.mygit.model.Language;
import com.example.cc.mygit.view.language.LanguageActivity;

import java.util.ArrayList;

import butterknife.OnClick;

/**
 * Created by cc on 2016/11/23.
 */

public class LanguageViewPagerFragment extends BaseViewPagerFragment implements View.OnClickListener {

    private ArrayList<Language> mLanguages = new ArrayList<>();

    public static LanguageViewPagerFragment newInstance() {
        return new LanguageViewPagerFragment();
    }


    @Override
    protected int getLayoutId() {
        return R.layout.fragment_language_view_pager;
    }

    @Override
    protected void initData() {
        super.initData();
        Language java = new Language();
        java.setId(5);
        java.setName("Java");
        mLanguages.add(java);

        Language android = new Language();
        android.setId(71);
        android.setName("android");
        mLanguages.add(android);

        mAdapter.notifyDataSetChanged();

    }

    @OnClick({R.id.ib_add_language})
    @Override
    public void onClick(View v) {
        LanguageActivity.show(this, mLanguages, 1);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null && resultCode == AppCompatActivity.RESULT_OK){
            mAdapter.setUpdateFlag(true);
            mLanguages = (ArrayList<Language>) data.getExtras().getSerializable("languages");
            mAdapter.notifyDataSetChanged();
        }
        mAdapter.setUpdateFlag(false);
    }

    @Override
    protected Fragment getItem(int position) {
        return LanguageProjectFragment.newInstance(mLanguages.get(position));
    }

    @Override
    protected int getCount() {
        return mLanguages.size();
    }

    @Override
    protected CharSequence getPageTitle(int position) {
        return mLanguages.get(position).getName();
    }
}
