package com.example.cc.mygit.view.code;

import android.os.Bundle;

import com.example.cc.mygit.base.BaseRecyclerFragment;
import com.example.cc.mygit.model.Code;
import com.example.cc.mygit.model.Project;
import com.example.cc.mygit.view.code.detail.CodeDetailActivity;
import com.haibin.androidbase.adapter.BaseRecyclerAdapter;

import java.util.List;

/**
 * Created by cc on 2017/2/23.
 */

public class CodeFragment extends BaseRecyclerFragment<CodeContract.Presenter, Code>
        implements CodeContract.View {

    private Project mProject;

    public static CodeFragment newInstance(Project mProject) {
        CodeFragment fragment = new CodeFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("project", mProject);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected void initData() {
        super.initData();
        mProject = (Project) getArguments().getSerializable("project");
    }


    @Override
    public void onItemClick(int position, long itemId) {

        if (((CodePresenter) mPresenter).isLoading)
            return;
        Code code = mAdapter.getItem(position);
        if (code == null)
            return;;
        if (Code.CODE_TYPE_FOLDER.equalsIgnoreCase(code.getType()))
            ((CodePresenter) mPresenter).load(code.getName());
        else
            CodeDetailActivity.show(mContext,
                    mProject.getId(),
                    ((CodePresenter) mPresenter).getPath() + code.getName(),
                    mProject.getDefaultBranch());
    }

    @Override
    public void onPreLoadSuccess(List<Code> codes) {
        mAdapter.resetItem(codes);
    }

    @Override
    protected BaseRecyclerAdapter<Code> getAdapter() {
        return new CodeAdapter(mContext);
    }
}
