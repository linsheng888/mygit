package com.example.cc.mygit.view.issue;

import android.content.Context;

import com.example.cc.mygit.R;
import com.example.cc.mygit.model.Issue;
import com.haibin.androidbase.adapter.BaseRecyclerAdapter;
import com.haibin.androidbase.adapter.ViewHolder;
import com.haibin.androidbase.utils.DateTimeFormat;

import static com.haibin.androidbase.adapter.BaseRecyclerAdapter.ONLY_FOOTER;

/**
 * Created by cc on 2017/1/1.
 */

public class IssueAdapter extends BaseRecyclerAdapter<Issue> {
    public IssueAdapter(Context context) {
        super(context, ONLY_FOOTER);
    }

    @Override
    protected int getLayoutId(int viewType) {
        return R.layout.item_list_issue;
    }

    @Override
    protected void onBindDefaultViewHolder(ViewHolder holder, Issue item, int position) {

        holder.bindText(R.id.tv_title, item.getTitle());
        holder.bindText(R.id.tv_iid, String.format("#%s", item.getIid()));
        holder.bindText(R.id.tv_date, DateTimeFormat.format(item.getCreatedDate()));
        holder.bindText(R.id.tv_author, item.getAuthor().getName());
    }
}
