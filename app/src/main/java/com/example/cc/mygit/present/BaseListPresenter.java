package com.example.cc.mygit.present;

/**
 * Created by cc on 2016/11/12.
 */

public interface BaseListPresenter extends BasePresenter{

    void onRefreshing();

    void onLoadMore();

}
