package com.example.cc.mygit.base;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;

import com.example.cc.mygit.R;
import com.haibin.androidbase.fragment.BaseFragment;

import butterknife.Bind;

/**
 * Created by cc on 2016/11/12.
 */

public abstract class BaseViewPagerFragment extends BaseFragment {

    protected TabLayout mTabLayout;
    protected FragmentAdapter mAdapter;
    protected ViewPager mViewPager;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_base_view_pager;
    }

    @Override
    protected void initView() {
        super.initView();
        mViewPager = (ViewPager) mRootView.findViewById(R.id.viewPager);
        mTabLayout = (TabLayout) mRootView.findViewById(R.id.tabLayout);

        mAdapter = new FragmentAdapter(getChildFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return BaseViewPagerFragment.this.getItem(position);
            }

            @Override
            public int getCount() {
                return BaseViewPagerFragment.this.getCount();
            }

            @Override
            public CharSequence getPageTitle(int position) {
                return BaseViewPagerFragment.this.getPageTitle(position);
            }
        };
        mViewPager.setAdapter(mAdapter);
        mTabLayout.setupWithViewPager(mViewPager);

    }

    protected abstract Fragment getItem(int position);

    protected abstract int getCount();

    protected abstract CharSequence getPageTitle(int position);
}
