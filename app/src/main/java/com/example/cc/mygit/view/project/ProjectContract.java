package com.example.cc.mygit.view.project;

import com.example.cc.mygit.model.Project;
import com.example.cc.mygit.present.BaseListPresenter;
import com.example.cc.mygit.present.BaseListView;
import com.example.cc.mygit.present.BasePresenter;

/**
 * Created by cc on 2016/11/13.
 */

public interface ProjectContract {

    interface View extends BaseListView<Presenter,Project>{
        void showToastShort(String string);
    }

    interface Presenter extends BaseListPresenter{

    }
}
