package com.example.cc.mygit.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by cc on 2017/1/1.
 */

public class Label extends BaseModel{

    private long id;
    private String name;
    private String color;
    @SerializedName("project_id")
    private long projectId;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public long getProjectId() {
        return projectId;
    }

    public void setProjectId(long projectId) {
        this.projectId = projectId;
    }
}
