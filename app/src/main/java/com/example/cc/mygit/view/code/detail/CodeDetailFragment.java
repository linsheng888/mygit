package com.example.cc.mygit.view.code.detail;

import com.example.cc.mygit.R;
import com.example.cc.mygit.base.BaseWebFragment;
import com.example.cc.mygit.model.CodeDetail;
import com.example.cc.mygit.utils.MarkdownUtils;
import com.example.cc.mygit.utils.SourceEditor;

/**
 * Created by cc on 2017/2/24.
 */

public class CodeDetailFragment extends BaseWebFragment implements CodeDetailContract.View {

    private SourceEditor mEditor;

    public static CodeDetailFragment newInstance() {
        return new CodeDetailFragment();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_code_detail;
    }

    @Override
    protected void initData() {
        super.initData();
        mEditor = new SourceEditor(mWebView);
    }

    @Override
    public void showGetCodeDetailSuccess(String url, CodeDetail detail) {
        mEditor.setMarkdown(MarkdownUtils.isMarkdown(url));
        mEditor.setSource(url, detail);
    }

    @Override
    public void showGetCodeDetailError(int strId) {

    }

    @Override
    public void showNetworkError(int strId) {

    }

    @Override
    public void setPresenter(CodeDetailContract.Presenter presenter) {

    }
}
