package com.example.cc.mygit.view.message;

import android.content.Context;

import com.bumptech.glide.Glide;
import com.example.cc.mygit.R;
import com.example.cc.mygit.model.Message;
import com.haibin.androidbase.adapter.BaseRecyclerAdapter;
import com.haibin.androidbase.adapter.ViewHolder;

/**
 * Created by cc on 2016/11/12.
 */

public class MessageAdapter extends BaseRecyclerAdapter<Message>{
    public MessageAdapter(Context context) {
        super(context, ONLY_FOOTER);
    }

    @Override
    protected int getLayoutId(int viewType) {
        return R.layout.item_list_message;
    }

    @Override
    protected void onBindDefaultViewHolder(ViewHolder holder, Message item, int position) {

        Glide.with(mContext)
                .load(item.getProject().getOwner().getPortrait())
                .asBitmap()
                .placeholder(R.mipmap.icon_git)
                .into(holder.getImageView(R.id.civ_author));
        holder.bindText(R.id.tv_project_name,item.getProject().getName());
        holder.bindText(R.id.tv_author,item.getProject().getOwner().getName());
        holder.bindText(R.id.tv_count,String.valueOf(item.getProject().getNotifications().size()));
    }
}
