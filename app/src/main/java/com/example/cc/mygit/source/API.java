package com.example.cc.mygit.source;

import com.example.cc.mygit.model.Branch;
import com.example.cc.mygit.model.Code;
import com.example.cc.mygit.model.CodeDetail;
import com.example.cc.mygit.model.Commit;
import com.example.cc.mygit.model.Count;
import com.example.cc.mygit.model.Issue;
import com.example.cc.mygit.model.Language;
import com.example.cc.mygit.model.Message;
import com.example.cc.mygit.model.Readme;
import com.example.cc.mygit.view.branch.BranchService;
import com.example.cc.mygit.view.code.CodeService;
import com.example.cc.mygit.view.commit.CommitService;
import com.example.cc.mygit.view.detail.ProjectDetailService;
import com.example.cc.mygit.view.issue.IssueService;
import com.example.cc.mygit.view.language.LanguageService;
import com.example.cc.mygit.view.login.LoginService;
import com.example.cc.mygit.view.message.MessageService;
import com.example.cc.mygit.view.project.ProjectsService;
import com.example.cc.mygit.model.Project;
import com.example.cc.mygit.model.User;
import com.example.cc.mygit.view.readme.ReadmeService;
import com.example.cc.mygit.view.userInfo.userProjects.UserProjectsService;
import com.example.cc.mygit.view.userInfo.userStartProjects.UserStartProjectsService;
import com.example.cc.mygit.view.userInfo.userWatchProjects.UserWatchProjectsService;
import com.haibin.elegant.Elegant;
import com.haibin.elegant.call.CallBack;

import java.util.List;

/**
 * Created by cc on 2016/11/12.
 */

public class API {
    public static final Elegant mElegant = new Elegant();

    static {
        mElegant.registerApi("http://git.oschina.net/api/v3/");
    }

    /**
     * 登陆
     */
    public static void login(String email, String pwd, CallBack<User> callBack) {
        mElegant.from(LoginService.class)
                .login(email, pwd)
                .execute(callBack);
    }

    /**
     * 获取项目列表，featured 推荐 -- popular 热门  --  latest 最近更新
     */
    public static void getProjects(String type,
                                   int page,
                                   String token,
                                   CallBack<List<Project>> callBack) {
        mElegant.from(ProjectsService.class)
                .getProjectList(type, page, token)
                .execute(callBack);
    }



    /**
     * 获取不同用户项目列表...................
     */
    public static void getUserProjects(String uid,
                                       int page,
                                       String token,
                                       CallBack<List<Project>> callBack){
        mElegant.from(UserProjectsService.class)
                .getUserProjectsList(uid,page,token)
                .execute(callBack);
    }

    /**
     * 获取不同用户start项目列表...................
     */
    public static void getUserStartProjects(String uid,
                                            int page,
                                            String token,
                                            CallBack<List<Project>> callBack){
        mElegant.from(UserStartProjectsService.class)
                .getUserStartProjectsList(uid,page,token)
                .execute(callBack);
    }
    /**
     * 获取不同用户watch项目列表...................
     */
    public static void getUserWatchProjects(String uid,
                                       int page,
                                       String token,
                                       CallBack<List<Project>> callBack){
        mElegant.from(UserWatchProjectsService.class)
                .getUserWatchProjectsList(uid,page,token)
                .execute(callBack);
    }

    /**
     * 获取项目详情
     */
    public static void getProjectDetail(String namespace,
                                        String token,
                                        CallBack<Project> callBack) {
        mElegant.from(ProjectDetailService.class)
                .getProjectDetail(namespace, token)
                .execute(callBack);
    }

    /**
     * 获取项目详情
     */
    public static void getProjectDetail(long id,
                                        String token,
                                        CallBack<Project> callBack) {
        mElegant.from(ProjectDetailService.class)
                .getProjectDetail(id, token)
                .execute(callBack);
    }

    /**
     * start项目
     */
    public static void startProject(String project_path_with_namespace,
                                    String token,
                                    CallBack<Count> callBack) {
        mElegant.from(ProjectDetailService.class)
                .start(project_path_with_namespace, project_path_with_namespace, token)
                .execute(callBack);
    }

    /**
     * unstart项目
     */
    public static void unstartProject(String project_path_with_namespace,
                                      String token,
                                      CallBack<Count> callBack) {
        mElegant.from(ProjectDetailService.class)
                .unstart(project_path_with_namespace, project_path_with_namespace, token)
                .execute(callBack);
    }

    /**
     * watch
     */
    public static void watchProject(String project_path_with_namespace,
                                    String token,
                                    CallBack<Count> callBack) {
        mElegant.from(ProjectDetailService.class)
                .watch(project_path_with_namespace, project_path_with_namespace, token)
                .execute(callBack);
    }

    /**
     * unwatch项目
     */
    public static void unwatchProject(String project_path_with_namespace,
                                      String token,
                                      CallBack<Count> callBack) {
        mElegant.from(ProjectDetailService.class)
                .unwatch(project_path_with_namespace, project_path_with_namespace, token)
                .execute(callBack);
    }

    /**
     * 获取消息通知
     */
    public static void getMessages(String token,
                                   String filter,
                                   String type,
                                   CallBack<List<Message>> callBack) {
        mElegant.from(MessageService.class)
                .getMessages(token, filter, type)
                .execute(callBack);

    }


    /**
     * 获取语言列表
     */
    public static void getLanguages(CallBack<List<Language>> callBack) {
        mElegant.from(LanguageService.class)
                .getLanguages()
                .execute(callBack);
    }

    /**
     * 通过语言获取项目
     */
    public static void getProjects(String token,
                                   long id,
                                   int page,
                                   CallBack<List<Project>> callBack) {
        mElegant.from(LanguageService.class)
                .getProjectsByLanguage(token, id, page)
                .execute(callBack);
    }

    /**
     * 获取项目仓库代码
     */
    public static void getCodeRepository(long project_id,
                                         String path,
                                         String ref_name,
                                         String token,
                                         CallBack<List<Code>> callBack) {
        mElegant.from(CodeService.class)
                .getCodeRepository(project_id, path, ref_name, token)
                .execute(callBack);
    }

    /**
     * 获取项目代码详情
     */
    public static void getCodeDetail(long project_id,
                                     String path,
                                     String ref,
                                     CallBack<CodeDetail> callBack) {
        mElegant.from(CodeService.class)
                .getCodeDetail(project_id, path, ref)
                .execute(callBack);
    }

    /**
     * 获取项目issue
     */
    public static void getIssues(long project_id,
                                 String token,
                                 int page,
                                 CallBack<List<Issue>> callBack) {
        mElegant.from(IssueService.class)
                .getIssues(project_id, token, page)
                .execute(callBack);
    }

    /**
     * 获取项目分支
     */
    public static void getBranchs(long project_id,
                                  String token,
                                  CallBack<List<Branch>> callBack) {
        mElegant.from(BranchService.class)
                .getBranches(project_id, token)
                .execute(callBack);
    }

    /**
     * 获取项目分支
     */
    public static void getTags(String project_owner_username,
                               String project_name,
                               String token,
                               CallBack<List<Branch>> callBack) {
        mElegant.from(BranchService.class)
                .getTags(project_owner_username, project_name, token)
                .execute(callBack);
    }


    /**
     * 获取项目提交列表
     */
    public static void getBranchs(long project_id,
                                  String token,
                                  int page,
                                  CallBack<List<Commit>> callBack) {
        mElegant.from(CommitService.class)
                .getCommits(project_id, token, page)
                .execute(callBack);
    }

    /**
     * 获得readme详情
     */
    public static void getReadme(String project_path_with_namespace,
                                 String token,
                                 CallBack<Readme> callBack) {
        mElegant.from(ReadmeService.class)
                .getReadme(project_path_with_namespace, token)
                .execute(callBack);
    }

}
