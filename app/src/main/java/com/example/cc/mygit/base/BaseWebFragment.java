package com.example.cc.mygit.base;

import android.annotation.SuppressLint;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.example.cc.mygit.R;
import com.haibin.androidbase.fragment.BaseFragment;

/**
 * Created by cc on 2016/12/13.
 */

public abstract class BaseWebFragment extends BaseFragment{

    protected WebView mWebView;
    protected String mHtml;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void initView() {
        super.initView();
        //为什么findViewById(R.id.webView)直接跳到layout (fragment_readme)
        mWebView = (WebView)mRootView.findViewById(R.id.webView);
        WebSettings settings = mWebView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setDefaultFontSize(10);
        settings.setAllowContentAccess(true);
        mWebView.setWebChromeClient(new WebChromeClient(){

        });
        mWebView.addJavascriptInterface(new JavaScriptInterface(),"git@osc");
    }
    final public class JavaScriptInterface{
        public JavaScriptInterface(){

        }

        @JavascriptInterface
        public void git(){

        }
        @JavascriptInterface
        public void showImage(String src){

        }
        @JavascriptInterface
        @Override
        public String toString() {
            return "listener";
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mWebView.destroy();
    }
}
