package com.example.cc.mygit.view.message;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.example.cc.mygit.R;
import com.example.cc.mygit.model.Message;
import com.example.cc.mygit.model.Notification;
import com.haibin.androidbase.activity.BaseRecyclerViewActivity;
import com.haibin.androidbase.adapter.BaseRecyclerAdapter;

/**
 * Created by cc on 2016/11/13.
 */

public class NotificationActivity extends BaseRecyclerViewActivity<Notification> {

    private Message mMessage;

    public static void show(Context context, Message message) {
        Intent intent = new Intent(context, NotificationActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("message", message);
        intent.putExtras(bundle);
        context.startActivity(intent);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_message_detail;
    }

    @Override
    protected void initData() {
        mMessage = (Message) getSerializable("message");
        super.initData();
        mAdapter.addAll(mMessage.getProject().getNotifications());
        mAdapter.setFooterState(BaseRecyclerAdapter.FOOTER_STATE_NO_MORE,true);
        mSuperRefreshLayout.setNoMoreData();
        setTitle(mMessage.getProject().getName());

    }

    @Override
    public void onRefreshing() {
        mSuperRefreshLayout.setRefreshing(false);
    }

    @Override
    protected BaseRecyclerAdapter<Notification> getAdapter() {
        return new NotificationAdapter(this);
    }

    @Override
    public void onLoadMore() {

    }
}
