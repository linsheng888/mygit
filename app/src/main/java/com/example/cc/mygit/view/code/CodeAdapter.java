package com.example.cc.mygit.view.code;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

import com.example.cc.mygit.R;
import com.example.cc.mygit.model.Code;
import com.haibin.androidbase.adapter.BaseRecyclerAdapter;
import com.haibin.androidbase.adapter.ViewHolder;

/**
 * Created by cc on 2017/1/25.
 */

public class CodeAdapter extends BaseRecyclerAdapter<Code> {
    public CodeAdapter(Context context) {
        super(context, NEITHER);
    }

    @Override
    protected int getLayoutId(int viewType) {
        return R.layout.item_list_code;
    }

    @Override
    protected void onBindDefaultViewHolder(ViewHolder holder, Code item, int position) {
        holder.setImageResource(R.id.iv_code,
                Code.CODE_TYPE_FOLDER.equalsIgnoreCase(item.getType()) ?
                        R.mipmap.icon_folder : R.mipmap.icon_file);
        holder.bindText(R.id.tv_code, item.getName());
    }

}
