package com.example.cc.mygit.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by cc on 2017/5/1.
 */

public class Events implements Serializable {

    @SerializedName("issue")
    private Issue _issue;

    @SerializedName("note")
    private Note _note;

    @SerializedName("pull_request")
    private Pull_request _pull_request;

    public Issue getIssue() {
        return _issue;
    }

    public void setIssue(Issue issue) {
        this._issue = issue;
    }

    public Note getNote() {
        return _note;
    }

    public void setNote(Note note) {
        this._note = note;
    }

    public Pull_request getPull_request() {
        return _pull_request;
    }

    public void setPull_request(Pull_request pull_request) {
        this._pull_request = pull_request;
    }
}
