package com.example.cc.mygit.view.message;

import android.support.v4.app.Fragment;

import com.example.cc.mygit.base.BaseViewPagerFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cc on 2016/11/13.
 */

public class MessageViewPagerFragment extends BaseViewPagerFragment {

    private List<Fragment> mFragments = new ArrayList<>();

    public static MessageViewPagerFragment newInstance() {
        return new MessageViewPagerFragment();
    }

    @Override
    protected void initView() {
        super.initView();
    }

    @Override
    protected void initData() {
        mFragments.add(MessageFragment.newInstance("0"));
        mFragments.add(MessageFragment.newInstance("1"));
    }

    @Override
    protected Fragment getItem(int position) {
        return mFragments.get(position);
    }

    @Override
    protected int getCount() {
        return 2;
    }

    @Override
    protected CharSequence getPageTitle(int position) {
        if (position == 0)
            return "未读消息";
        return "已读消息";
    }
}
