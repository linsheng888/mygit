package com.example.cc.mygit.view.language.project;

import android.os.Bundle;

import com.example.cc.mygit.base.BaseRecyclerFragment;
import com.example.cc.mygit.model.Language;
import com.example.cc.mygit.model.Project;
import com.example.cc.mygit.view.detail.ProjectDetailActivity;
import com.example.cc.mygit.view.project.ProjectAdapter;
import com.haibin.androidbase.adapter.BaseRecyclerAdapter;

/**
 * Created by cc on 2016/11/23.
 */

public class LanguageProjectFragment extends BaseRecyclerFragment<LanguageProjectContract.Presenter,Project> implements LanguageProjectContract.View{

   public static LanguageProjectFragment newInstance(Language language){
       LanguageProjectFragment fragment = new LanguageProjectFragment();
       Bundle bundle = new Bundle();
       bundle.putSerializable("language",language);
       fragment.setArguments(bundle);
       return fragment;
   }

    @Override
    protected void initData() {
        new LanguageProjectPresenter(this,(Language) getArguments().getSerializable("language")).start();
        super.initData();
    }

    @Override
    public void onItemClick(int position, long itemId) {
        ProjectDetailActivity.show(this,mAdapter.getItem(position),1);

    }

    @Override
    protected BaseRecyclerAdapter<Project> getAdapter() {
        return new ProjectAdapter(getActivity());
    }
}
