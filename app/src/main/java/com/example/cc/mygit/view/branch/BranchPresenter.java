package com.example.cc.mygit.view.branch;

import com.example.cc.mygit.GitApplication;
import com.example.cc.mygit.model.Branch;
import com.example.cc.mygit.model.Project;
import com.example.cc.mygit.model.User;
import com.example.cc.mygit.source.API;
import com.haibin.elegant.Response;
import com.haibin.elegant.call.CallBack;

import java.util.List;

/**
 * Created by cc on 2017/2/22.
 */

public class BranchPresenter implements BranchContract.Presenter {

    private final BranchContract.View mView;
    private User mUser;
    private Project mProject;


    public BranchPresenter(BranchContract.View mView, Project mProject) {
        this.mView = mView;
        mUser = GitApplication.getConfig().getUser();
        this.mProject = mProject;
        this.mView.setPresenter(this);
    }

    @Override
    public void onRefreshing() {
        API.getBranchs(mProject.getId(),
                mUser.getPrivateToken(),
                new CallBack<List<Branch>>() {
                    @Override
                    public void onResponse(Response<List<Branch>> response) {
                        List<Branch> branches = response.getBody();
                        if (branches != null) {
                            mView.onRefreshSuccess(branches);
                            if (branches.size() < 20){
                                mView.showNoMoreData();
                            }
                        }else {
                            mView.showNetWorkError();
                        }
                        mView.onComplete();
                    }

                    @Override
                    public void onFailure(Exception e) {
                        mView.showNetWorkError();
                        mView.onComplete();

                    }
                });

    }

    @Override
    public void onLoadMore() {

    }

    @Override
    public void start() {

    }
}
