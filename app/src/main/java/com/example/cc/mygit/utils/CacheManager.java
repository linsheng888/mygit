package com.example.cc.mygit.utils;

import android.content.Context;

/**
 * Created by cc on 2016/12/13.
 */

public class CacheManager {
    public static String CACHE_DIR;
    public static String FILE_DIR;

    public static void init(Context context) {
        CACHE_DIR = context.getCacheDir().getPath();
        FILE_DIR = context.getFilesDir().getPath();
    }

}
