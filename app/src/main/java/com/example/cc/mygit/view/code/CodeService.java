package com.example.cc.mygit.view.code;

import com.example.cc.mygit.model.Code;
import com.example.cc.mygit.model.CodeDetail;
import com.haibin.elegant.call.Call;
import com.haibin.elegant.net.Form;
import com.haibin.elegant.net.GET;
import com.haibin.elegant.net.Path;

import java.util.List;

/**
 * Created by cc on 2017/2/23.
 */

public interface CodeService {


    /**
     * 获取项目仓库代码
     *
     * @param path     仓库的相对路径 如：app/src/main
     * @param ref_name 分支或者标签名称，默认为master分支
     */
    @GET("projects/{project_id}/repository/tree")
    Call<List<Code>> getCodeRepository(@Path("project_id") long project_id,
                                       @Form("path") String path,
                                       @Form("ref_name") String ref_name,
                                       @Form("private_token") String token);

    /**
     * 获取用户项目仓库代码
     *
     * @param project_owner_username 项目作者的username
     * @param project_name           项目的name
     * @param path                   仓库的相对路径 如：app/src/main
     * @param ref_name               分支或者标签名称，默认为master分支
     */
    @GET("projects/{project_owner_username}%2F{project_name}/repository/tree")
    Call<List<Code>> getUserCodeRepository(@Path("project_owner_username") String project_owner_username,
                                           @Path("project_name") String project_name,
                                           @Form("path") String path,
                                           @Form("ref_name") String ref_name,
                                           @Form("private_token") String token);

    /**
     * 获取代码详情
     *
     * @param path 仓库的相对路径 如：app/src/main
     * @param ref  分支或者标签名称，默认为master分支
     */
    @GET("projects/{project_id}/repository/files")
    Call<CodeDetail> getCodeDetail(@Path("project_id") long project_id,
                                   @Form("file_path") String path,
                                   @Form("ref") String ref);

    /**
     * 获取用户代码详情
     *
     * @param project_owner_username 项目作者的username
     * @param project_name           项目的name
     * @param path                   仓库的相对路径 如：app/src/main
     * @param ref                    分支或者标签名称，默认为master分支
     */
    @GET("projects/{project_owner_username}%2F{project_name}/repository/files")
    Call<Code> getUserCodeDetail(@Path("project_owner_username") String project_owner_username,
                                 @Path("project_name") String project_name,
                                 @Form("file_path") String path,
                                 @Form("ref") String ref,
                                 @Form("private_token") String token);

}
