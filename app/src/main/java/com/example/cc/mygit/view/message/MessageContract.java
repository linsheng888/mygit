package com.example.cc.mygit.view.message;

import com.example.cc.mygit.model.Message;
import com.example.cc.mygit.present.BaseListPresenter;
import com.example.cc.mygit.present.BaseListView;

/**
 * Created by cc on 2016/11/13.
 */

public interface MessageContract {

    interface View extends BaseListView<Presenter,Message>{


    }

    interface Presenter extends BaseListPresenter{

    }

}
