package com.example.cc.mygit.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by cc on 2017/5/1.
 */

public class Pull_request extends Entity{

    @SerializedName("iid")
    private int _iid;

    @SerializedName("title")
    private String _title;

    public int getIid() {
        return _iid;
    }

    public void setIid(int iid) {
        this._iid = iid;
    }

    public String getTitle() {
        return _title;
    }

    public void setTitle(String title) {
        this._title = title;
    }
}
