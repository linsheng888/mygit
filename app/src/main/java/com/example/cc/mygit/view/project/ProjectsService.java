package com.example.cc.mygit.view.project;

import com.example.cc.mygit.model.Project;
import com.haibin.elegant.call.Call;
import com.haibin.elegant.net.Form;
import com.haibin.elegant.net.GET;
import com.haibin.elegant.net.Path;

import java.util.List;

/**
 * Created by cc on 2016/11/13.
 */

public interface ProjectsService {
    /**
     * 获取项目列表，featured 推荐 -- popular 热门  --  latest 最近更新
     */
    @GET("projects/{path}")
    Call<List<Project>> getProjectList(@Path("path") String path,
                                       @Form("page") int page,
                                       @Form("private_token") String token);

}
