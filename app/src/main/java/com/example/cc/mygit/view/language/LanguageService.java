package com.example.cc.mygit.view.language;

import com.example.cc.mygit.model.Language;
import com.example.cc.mygit.model.Project;
import com.haibin.elegant.call.Call;
import com.haibin.elegant.net.File;
import com.haibin.elegant.net.Form;
import com.haibin.elegant.net.GET;
import com.haibin.elegant.net.Path;

import java.util.List;

/**
 * Created by cc on 2016/11/23.
 */

public interface LanguageService {
    /**
     * 获取语言列表
     */
    @GET("projects/languages")
    Call<List<Language>> getLanguages();

    /**
     * 通过语言获取项目
     */
    @GET("projects/languages/{id}")
    Call<List<Project>> getProjectsByLanguage(@Form("private_token") String token,
                                              @Path("id") long id,
                                              @File("page") int page);
}
