package com.example.cc.mygit.view.language;

import com.example.cc.mygit.model.Language;
import com.example.cc.mygit.present.BaseListPresenter;
import com.example.cc.mygit.present.BaseListView;

/**
 * Created by cc on 2016/11/13.
 */

public interface LanguageContract {

    interface View extends BaseListView<Presenter,Language>{

    }

    interface Presenter extends BaseListPresenter{

    }
}
