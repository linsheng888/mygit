package com.example.cc.mygit.view.issue;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;

import com.example.cc.mygit.R;
import com.example.cc.mygit.base.BaseRecyclerActivity;
import com.example.cc.mygit.model.Issue;
import com.example.cc.mygit.model.Project;
import com.haibin.androidbase.activity.BaseBackActivity;
import com.haibin.androidbase.adapter.BaseRecyclerAdapter;

/**
 * Created by cc on 2016/12/13.
 */

public class IssueActivity extends BaseRecyclerActivity<IssueContract.Presenter, Issue>  implements IssueContract.View{

    private Project mProject;

    public static void show(Context context, Project project) {
        Intent intent = new Intent(context, IssueActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("project", project);
        intent.putExtras(bundle);
        context.startActivity(intent);
    }

    @Override
    protected void initData() {
        mProject = (Project) getIntent().getExtras().getSerializable("project");
        new IssuePresenter(this, mProject).start();
        super.initData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add_issue, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    protected BaseRecyclerAdapter<Issue> getAdapter() {
        return new IssueAdapter(this);
    }

}
