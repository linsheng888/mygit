package com.example.cc.mygit.view.issue;

import com.example.cc.mygit.model.Issue;
import com.example.cc.mygit.present.BaseListPresenter;
import com.example.cc.mygit.present.BaseListView;

/**
 * Created by cc on 2017/1/1.
 */

public interface IssueContract {

    interface View extends BaseListView<Presenter, Issue> {

    }

    interface Presenter extends BaseListPresenter {

    }
}
