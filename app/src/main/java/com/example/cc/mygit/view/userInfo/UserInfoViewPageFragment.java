package com.example.cc.mygit.view.userInfo;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.example.cc.mygit.base.BaseViewPagerFragment;
import com.example.cc.mygit.model.Project;
import com.example.cc.mygit.model.User;
import com.example.cc.mygit.view.project.ProjectFragment;
import com.example.cc.mygit.view.userInfo.userProjects.UserProjectsFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cc on 2017/3/18.
 */

public class UserInfoViewPageFragment extends BaseViewPagerFragment {

    private Project mProject;
    private User mUser;

    private List<Fragment> mFragments = new ArrayList<>();

    public static UserInfoViewPageFragment newInstance(Project project) {
        UserInfoViewPageFragment fragment = new UserInfoViewPageFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("project", project);
        fragment.setArguments(bundle);
        return fragment;
    }

    public static UserInfoViewPageFragment newInstance(User user) {
        UserInfoViewPageFragment fragment = new UserInfoViewPageFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("user", user);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected void initData() {
        //公用同一个UserProjectsFragment，传入不同的type,new 不同的Presenter；
        String type1 = "userProjects";
        String type2 = "start";
        String type3 = "watch";

        mProject = (Project) getArguments().getSerializable("project");
        mUser = (User) getArguments().getSerializable("user");

        if (mProject != null) {
            //从点击items中获取user信息
            mFragments.add(ProjectFragment.newInstance("featured"));
            mFragments.add(UserProjectsFragment.newInstance(mProject,type1));
            mFragments.add(UserProjectsFragment.newInstance(mProject,type2));
            mFragments.add(UserProjectsFragment.newInstance(mProject,type3));
        } else {
            //直接获取自己的信息
            mFragments.add(ProjectFragment.newInstance("featured"));
            mFragments.add(UserProjectsFragment.newInstance(mUser,type1));
            mFragments.add(UserProjectsFragment.newInstance(mUser,type2));
            mFragments.add(UserProjectsFragment.newInstance(mUser,type3));
        }

    }

    @Override
    protected Fragment getItem(int position) {
        return mFragments.get(position);
    }

    @Override
    protected int getCount() {
        return 4;
    }

    @Override
    protected CharSequence getPageTitle(int position) {
        if (position == 0)
            return "最近动态";
        if (position == 1)
            return "项目";
        if (position == 2)
            return "Start";
        return "Watch";
    }
}
