package com.example.cc.mygit.model;

/**
 * Created by cc on 2016/11/12.
 */

public class Message extends BaseModel {
    private Project project;

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }
}
