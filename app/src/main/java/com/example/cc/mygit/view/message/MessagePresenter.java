package com.example.cc.mygit.view.message;

import com.example.cc.mygit.GitApplication;
import com.example.cc.mygit.model.Message;
import com.example.cc.mygit.source.API;
import com.google.gson.annotations.SerializedName;
import com.haibin.elegant.Response;
import com.haibin.elegant.call.CallBack;

import java.util.List;

/**
 * Created by cc on 2016/11/13.
 */

public class MessagePresenter implements MessageContract.Presenter {

    private final MessageContract.View mView;
    private String mType;

    public MessagePresenter(MessageContract.View mView, String mType) {
        this.mView = mView;
        this.mType = mType;
        this.mView.setPresenter(this);
    }


    @Override
    public void onRefreshing() {
        API.getMessages(GitApplication.getConfig().getUser().getPrivateToken(), "", mType, new CallBack<List<Message>>() {
            @Override
            public void onResponse(Response<List<Message>> response) {
                List<Message> items = response.getBody();
                if (items != null){
                    mView.onRefreshSuccess(items);
                    mView.showNoMoreData();
                } else {
                    mView.onRefreshFailure();
                }
                mView.onComplete();
            }

            @Override
            public void onFailure(Exception e) {
                mView.showNetWorkError();
                mView.onComplete();
            }
        });

    }

    @Override
    public void onLoadMore() {

    }

    @Override
    public void start() {

    }
}
