package com.example.cc.mygit.utils;

import android.content.Context;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;

/**
 * Created by cc on 2016/12/13.
 */

public class ImageLoader {
    private static RequestManager mManager;

    public static void init(Context context) {
        mManager = Glide.with(context);
    }

    public static RequestManager getGlide() {
        return mManager;
    }
}
