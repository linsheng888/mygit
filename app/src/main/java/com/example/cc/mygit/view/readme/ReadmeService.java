package com.example.cc.mygit.view.readme;

import com.example.cc.mygit.model.Readme;
import com.haibin.elegant.call.Call;
import com.haibin.elegant.net.Form;
import com.haibin.elegant.net.GET;
import com.haibin.elegant.net.Path;

/**
 * Created by cc on 2017/1/1.
 */

public interface ReadmeService {

    /**
     * unstart项目
     */
    @GET("projects/{project_path_with_namespace}/readme")
    Call<Readme> getReadme(@Path("project_path_with_namespace") String project_path_with_namespace,
                           @Form("private_token") String token);
}
