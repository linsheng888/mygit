package com.example.cc.mygit.view.project;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;

import com.bumptech.glide.Glide;
import com.example.cc.mygit.R;
import com.example.cc.mygit.model.Project;
import com.haibin.androidbase.adapter.BaseRecyclerAdapter;
import com.haibin.androidbase.adapter.ViewHolder;

/**
 * Created by cc on 2016/11/12.
 */

public class ProjectAdapter extends BaseRecyclerAdapter<Project> {
    public ProjectAdapter(Context context) {
        super(context, ONLY_FOOTER);
    }

    @Override
    protected int getLayoutId(int viewType) {
        return R.layout.item_list_project;
    }

    @Override
    protected void onBindDefaultViewHolder(ViewHolder holder, Project item, int position) {

        holder.bindText(R.id.tv_name, item.getOwner().getName() + "/" + item.getName());
        if (TextUtils.isEmpty(item.getDescription())) {
            holder.bindText(R.id.tv_content, R.string.not_description);
        } else {
            holder.bindText(R.id.tv_content, item.getDescription());
        }

        Glide.with(mContext)
                .load(item.getOwner().getNewPortrait())
                .asBitmap()
                .placeholder(R.mipmap.icon_git)
                .into(holder.getImageView(R.id.civ_author));

        holder.bindText(R.id.tv_watched, String.valueOf(item.getWatchesCount()));
        holder.bindText(R.id.tv_start, String.valueOf(item.getStarsCount()));
        holder.bindText(R.id.tv_fork, String.valueOf(item.getForksCount()));
        holder.bindText(R.id.tv_language, item.getLanguage());
    }
}
