package com.example.cc.mygit.view.readme;

import android.text.TextUtils;

import com.example.cc.mygit.GitApplication;
import com.example.cc.mygit.R;
import com.example.cc.mygit.model.Readme;
import com.example.cc.mygit.model.User;
import com.example.cc.mygit.source.API;
import com.haibin.elegant.Response;
import com.haibin.elegant.call.CallBack;

/**
 * Created by cc on 2017/1/1.
 */

public class ReadmePresenter implements ReadmeContract.Presenter {

    private final ReadmeContract.View mView;
    private User mUser;
    private String mHtml;

    public ReadmePresenter(ReadmeContract.View mView, String mHtml) {
        this.mView = mView;
        this.mUser = GitApplication.getConfig().getUser();
        this.mHtml = mHtml;
        this.mView.setPresenter(this);
    }

    @Override
    public void getReadme(String project_namespace) {
        API.getReadme(project_namespace.replace("/", "%2F").replace(".", "+"),
                mUser.getPrivateToken(),
                new CallBack<Readme>() {
                    @Override
                    public void onResponse(Response<Readme> response) {
                        Readme readme = response.getBody();
                        if (readme != null) {
                            readme.setContent(mHtml.replace("{content}", TextUtils.isEmpty(readme.getContent()) ? "NOT README" : readme.getContent()));
                            mView.showGetReadmeSuccess(readme);
                        }else{
                            mView.showGetReadmeError(R.string.readme);
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {

                    }
                });
    }

    @Override
    public void start() {

    }
}
