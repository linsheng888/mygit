package com.example.cc.mygit.view.branch;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.example.cc.mygit.R;
import com.example.cc.mygit.model.Branch;
import com.example.cc.mygit.model.Project;
import com.haibin.androidbase.adapter.BaseRecyclerAdapter;

import java.util.List;

/**
 * Created by cc on 2017/2/22.
 */

public class BranchDialog extends BottomSheetDialog implements BranchContract.View{

    private RecyclerView mRecyclerView;
    private BranchAdapter mAdapter;
    private  BranchPresenter mPresenter;
    private Project mProject;
    private BottomSheetBehavior mBehavior;

    public BranchDialog(@NonNull Context context, Project project) {
        super(context);
        this.mProject = project;
        init(context);
    }

    private void init(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.bottom_sheet_branch,null);
        mRecyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        mAdapter = new BranchAdapter(context);
        mRecyclerView.setAdapter(mAdapter);
        setContentView(view);

        Window window = getWindow();
        if (window != null){
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
        mBehavior = BottomSheetBehavior.from(findViewById(android.support.design.R.id.design_bottom_sheet));
        mBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_HIDDEN){
                    dismiss();
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });
        mAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position, long itemId) {
                mAdapter.update(position);
            }
        });
        mPresenter = new BranchPresenter(this,mProject);
        mPresenter.start();
        mPresenter.onRefreshing();

    }

    @Override
    public void show() {
        super.show();
        mBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }

    @Override
    public void onRefreshSuccess(List<Branch> items) {
        mAdapter.resetItem(items);
    }

    @Override
    public void onLoadMoreSuccess(List<Branch> items) {
        mAdapter.addAll(items);
    }

    @Override
    public void showNetWorkError() {
        mAdapter.setFooterState(BaseRecyclerAdapter.FOOTER_STATE_INVALID_NETWORK,true);
    }

    @Override
    public void onComplete() {

    }

    @Override
    public void onRefreshFailure() {

    }

    @Override
    public void showNoMoreData() {
        mAdapter.setFooterState(BaseRecyclerAdapter.FOOTER_STATE_NO_MORE,true);
    }

    @Override
    public void setPresenter(BranchContract.Presenter presenter) {

    }
}
