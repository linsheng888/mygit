package com.example.cc.mygit.view.userInfo.userWatchProjects;

import com.example.cc.mygit.model.Project;
import com.haibin.elegant.call.Call;
import com.haibin.elegant.net.Form;
import com.haibin.elegant.net.GET;
import com.haibin.elegant.net.Path;

import java.util.List;

/**
 * Created by cc on 2017/3/18.
 */

public interface UserWatchProjectsService {

    /**
     * 获取用户watch项目列表
     */
    @GET("user/{uid}/watched_projects")
    Call<List<Project>> getUserWatchProjectsList(@Path("uid") String uid,
                                                 @Form("page") int page,
                                                 @Form("private_token") String token);
}
