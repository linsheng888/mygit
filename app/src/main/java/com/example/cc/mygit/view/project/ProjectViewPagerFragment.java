package com.example.cc.mygit.view.project;

import android.support.v4.app.Fragment;

import com.example.cc.mygit.base.BaseViewPagerFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cc on 2016/11/13.
 */

public class ProjectViewPagerFragment extends BaseViewPagerFragment {

    private List<Fragment> mFragments = new ArrayList<>();

    public static ProjectViewPagerFragment newInstance(){
        return new ProjectViewPagerFragment();
    }

    @Override
    protected void initData() {
        mFragments.add(ProjectFragment.newInstance("featured"));
        mFragments.add(ProjectFragment.newInstance("popular"));
        mFragments.add(ProjectFragment.newInstance("latest"));
    }

    @Override
    protected Fragment getItem(int position) {
        return mFragments.get(position);
    }

    @Override
    protected int getCount() {
        return 3;
    }

    @Override
    protected CharSequence getPageTitle(int position) {
        if (position == 0)
            return "推荐项目";
        if (position == 1)
            return "热门项目";
        return "最近更新";
    }
}
