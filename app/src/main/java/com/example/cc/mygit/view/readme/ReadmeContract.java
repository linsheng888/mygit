package com.example.cc.mygit.view.readme;

import com.example.cc.mygit.model.Readme;
import com.example.cc.mygit.present.BasePresenter;
import com.example.cc.mygit.present.BaseView;

/**
 * Created by cc on 2017/1/1.
 */

public interface ReadmeContract {

    interface View extends BaseView<Presenter>{
        void showGetReadmeSuccess(Readme readme);

        void showGetReadmeError(int strId);

        void showNetworkError(int strId);
    }

    interface Presenter extends BasePresenter{
        void getReadme(String Project_namespace);
    }

}
