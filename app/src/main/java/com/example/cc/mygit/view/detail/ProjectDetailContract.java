package com.example.cc.mygit.view.detail;

import com.example.cc.mygit.model.Project;
import com.example.cc.mygit.present.BasePresenter;
import com.example.cc.mygit.present.BaseView;

/**
 * Created by cc on 2016/11/13.
 */

public interface ProjectDetailContract {

    interface ActionView{
        void onStarSuccess(int count);

        void onStartFailure(int strId);

        void onUnStartSuccess(int count);

        void onUnStartFailure(int strId);

        void onWatchSuccess(int count);

        void onWatchFailure(int strId);

        void onUnwatchSuccess(int count);

        void onUnwatchFailure(int strId);
    }

    interface View extends BaseView<Presenter> {

        void onGetDetailSuccess(Project project);

        void showGetDetailFailure(int strId);

        void showNetworkError(int strId);
    }

    interface Presenter extends BasePresenter {

        void getProjectDetail(long id);

        void getProjectDetail(String namespace);

        void startOrUnStart();

        void watchOrUnwatch();
    }
}
