package com.example.cc.mygit.utils;

import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.InputStream;

/**
 * Created by cc on 2016/12/13.
 */

public class IO {
    public static void close(Closeable... closeables) {
        for (Closeable closeable : closeables) {
            if (closeable != null) {
                try {
                    closeable.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static String asString(InputStream is) {
        ByteArrayOutputStream os = null;
        String html = "";
        try {
            int read;
            byte[] bytes = new byte[1024];
            os = new ByteArrayOutputStream();
            while ((read = is.read(bytes)) != -1) {
                os.write(bytes, 0, read);
            }
            html = os.toString();
            os.close();
            is.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            com.haibin.androidbase.utils.IO.close(is, os);
        }
        return html;
    }
}
