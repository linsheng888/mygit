package com.example.cc.mygit.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by cc on 2016/11/12.
 */

public class Notification extends BaseModel{

    private long id;

    @SerializedName("user_id")
    private long userId;

    @SerializedName("target_id")
    private String targetId;

    @SerializedName("target_type")
    private String targetType;

    @SerializedName("project_id")
    private int projectId;

    private boolean participating;

    private boolean read;

    private boolean mute;

    @SerializedName("created_at")
    private Date createdDate;

    @SerializedName("updated_at")
    private Date updatedDate;

    private String title;

    @SerializedName("userinfo")
    private User userInfo;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getTargetId() {
        return targetId;
    }

    public void setTargetId(String targetId) {
        this.targetId = targetId;
    }

    public String getTargetType() {
        return targetType;
    }

    public void setTargetType(String targetType) {
        this.targetType = targetType;
    }

    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    public boolean isParticipating() {
        return participating;
    }

    public void setParticipating(boolean participating) {
        this.participating = participating;
    }

    public boolean isRead() {
        return read;
    }

    public void setRead(boolean read) {
        this.read = read;
    }

    public boolean isMute() {
        return mute;
    }

    public void setMute(boolean mute) {
        this.mute = mute;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public User getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(User userInfo) {
        this.userInfo = userInfo;
    }
}
