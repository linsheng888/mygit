package com.example.cc.mygit.view.message;

import android.content.Context;

import com.bumptech.glide.Glide;
import com.example.cc.mygit.R;
import com.example.cc.mygit.model.Notification;
import com.haibin.androidbase.adapter.BaseRecyclerAdapter;
import com.haibin.androidbase.adapter.ViewHolder;
import com.haibin.androidbase.utils.DateTimeFormat;

/**
 * Created by cc on 2016/11/13.
 */

public class NotificationAdapter extends BaseRecyclerAdapter<Notification>{

    public NotificationAdapter(Context context) {
        super(context, ONLY_FOOTER);
    }
    @Override
    protected int getLayoutId(int viewType) {
        return R.layout.item_list_notification;
    }

    @Override
    protected void onBindDefaultViewHolder(ViewHolder holder, Notification item, int position) {

        Glide.with(mContext)
                .load(item.getUserInfo().getPortrait())
                .asBitmap()
                .placeholder(R.mipmap.icon_git)
                .into(holder.getImageView(R.id.civ_author));
        holder.bindText(R.id.tv_author_name, item.getUserInfo().getName());
        holder.bindText(R.id.tv_email, item.getUserInfo().getEmail());
        holder.bindText(R.id.tv_title, item.getTitle());
        holder.bindText(R.id.tv_time, DateTimeFormat.format(item.getCreatedDate()));
    }
}
