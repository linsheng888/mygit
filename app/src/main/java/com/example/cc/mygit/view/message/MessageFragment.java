package com.example.cc.mygit.view.message;

import android.os.Bundle;

import com.example.cc.mygit.base.BaseRecyclerFragment;
import com.example.cc.mygit.model.Message;
import com.haibin.androidbase.adapter.BaseRecyclerAdapter;

/**
 * Created by cc on 2016/11/13.
 */

public class MessageFragment extends BaseRecyclerFragment<MessageContract.Presenter,Message> implements MessageContract.View{

    public static MessageFragment newInstance(String type){
        MessageFragment frament = new MessageFragment();
        Bundle bundle = new Bundle();
        bundle.putString("type",type);
        frament.setArguments(bundle);
        return frament;
    }

    @Override
    protected void initData() {
        new MessagePresenter(this,getArguments().getString("type")).start();
        super.initData();
    }

    @Override
    public void onItemClick(int position, long itemId) {
        NotificationActivity.show(getActivity(),mAdapter.getItem(position));
    }

    @Override
    public void onLoadMore() {

    }

    @Override
    protected BaseRecyclerAdapter<Message> getAdapter() {
        return new MessageAdapter(getActivity());
    }
}
