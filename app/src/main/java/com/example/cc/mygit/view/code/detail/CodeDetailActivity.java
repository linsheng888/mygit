package com.example.cc.mygit.view.code.detail;

import android.content.Context;
import android.content.Intent;

import com.example.cc.mygit.R;
import com.example.cc.mygit.model.Code;
import com.haibin.androidbase.activity.BaseBackActivity;

/**
 * Created by cc on 2017/1/25.
 */

public class CodeDetailActivity extends BaseBackActivity {

    private CodeDetailPresenter mPresenter;

    public static void show(Context context, long projectId, String path, String ref) {
        Intent intent = new Intent(context, CodeDetailActivity.class);
        intent.putExtra("projectId", projectId);
        intent.putExtra("path", path);
        intent.putExtra("ref", ref);
        context.startActivity(intent);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_code_detail;
    }

    @Override
    protected void initView() {
        super.initView();
        CodeDetailFragment fragment = CodeDetailFragment.newInstance();
        addFragment(R.id.fl_content, fragment);
        Intent intent = getIntent();
        mPresenter = new CodeDetailPresenter(fragment,
                intent.getLongExtra("projectId", 0),
                intent.getStringExtra("path"),
                intent.getStringExtra("ref"));
        mPresenter.start();
        mPresenter.getCodeDetail();
    }
}
