package com.example.cc.mygit.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by cc on 2016/11/12.
 */

public class Language extends BaseModel {
    private long id;

    private String detail;

    @SerializedName("created_at")
    private Date createDate;

    private String ident;

    private String name;

    private int order;

    private int parent_id;

    @SerializedName("projects_count")
    private  int projectsCount;

    @SerializedName("updated_at")
    private Date updateDate;
    private boolean isAdd;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getIdent() {
        return ident;
    }

    public void setIdent(String ident) {
        this.ident = ident;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public int getParent_id() {
        return parent_id;
    }

    public void setParent_id(int parent_id) {
        this.parent_id = parent_id;
    }

    public int getProjectsCount() {
        return projectsCount;
    }

    public void setProjectsCount(int projectsCount) {
        this.projectsCount = projectsCount;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public boolean isAdd(){
        return isAdd;
    }

    public void setAdd(boolean add) {
        isAdd = add;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Language)
            return ((Language)obj).getId() == id;
        return false;
    }
}
