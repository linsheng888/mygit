package com.example.cc.mygit.view.project;

import com.example.cc.mygit.GitApplication;
import com.example.cc.mygit.model.Project;
import com.example.cc.mygit.model.User;
import com.example.cc.mygit.source.API;
import com.example.cc.mygit.source.RetrofitAPI;
import com.example.cc.mygit.source.RetrofitService;
import com.haibin.elegant.Response;
import com.haibin.elegant.call.CallBack;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by cc on 2016/11/13.
 */

public class ProjectPresenter implements ProjectContract.Presenter {

    private String mType;
    private int mPage = 1;
    private final ProjectContract.View mView;
    private User mUser;

    public ProjectPresenter(ProjectContract.View mView, String mType) {
        this.mType = mType;
        this.mView = mView;
        this.mUser = GitApplication.getConfig().getUser();
        this.mView.setPresenter(this);
    }

    //Elegant请求网络数据
    @Override
    public void onRefreshing() {
//        API.getProjects(mType, 1, mUser.getPrivateToken(), new CallBack<List<Project>>() {
//            @Override
//            public void onResponse(Response<List<Project>> response) {
//                List<Project> items = response.getBody();
//                if (items != null) {
//                    mView.onRefreshSuccess(items);
//                    mPage = 1;
//                    ++mPage;
//                }
//                mView.onComplete();
//            }
//
//            @Override
//            public void onFailure(Exception e) {
//                mView.showNetWorkError();
//                mView.onComplete();
//            }
//        });
        rxRetrofitOnRefreshing();

    }

    //Retrofit请求网络数据
    protected void RetrofitOnRefreshing() {
        RetrofitAPI.getProjects(mType, 1, mUser.getPrivateToken(), new Callback<List<Project>>() {
            @Override
            public void onResponse(Call<List<Project>> call, retrofit2.Response<List<Project>> response) {
                List<Project> items = response.body();
                if (items != null) {
                    mView.onRefreshSuccess(items);
                    mPage = 1;
                    ++mPage;
                }
                mView.onComplete();
            }

            @Override
            public void onFailure(Call<List<Project>> call, Throwable t) {
                mView.showNetWorkError();
                mView.onComplete();
            }
        });
    }

    //Rxjava+Retrofit请求网络数据
    protected void rxRetrofitOnRefreshing() {
        Observable.create(new ObservableOnSubscribe<List<Project>>() {
            @Override
            public void subscribe(ObservableEmitter<List<Project>> e) throws Exception {
                e.onNext(RetrofitAPI.retrofit.create(RetrofitService.class)
                        .getProjectList(mType, 1, mUser.getPrivateToken())
                        .execute().body());
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<Project>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(List<Project> items) {
                        if (items != null) {
                            mView.onRefreshSuccess(items);
                            mPage = 1;
                            ++mPage;
                        }
                        mView.onComplete();
                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.showNetWorkError();
                        mView.onComplete();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    public void onLoadMore() {
        API.getProjects(mType, mPage, mUser.getPrivateToken(), new CallBack<List<Project>>() {
            @Override
            public void onResponse(Response<List<Project>> response) {
                List<Project> items = response.getBody();
                if (items != null) {
                    mView.onLoadMoreSuccess(items);
                    ++mPage;
                    if (items.size() < 20) {
                        mView.showNoMoreData();
                        --mPage;
                    }
                    mView.onComplete();
                }
            }

            @Override
            public void onFailure(Exception e) {
                mView.showNetWorkError();
                mView.onComplete();
            }
        });


    }

    @Override
    public void start() {

    }
}
