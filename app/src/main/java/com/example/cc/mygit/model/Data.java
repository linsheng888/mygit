package com.example.cc.mygit.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by cc on 2017/5/1.
 */

public class Data implements Serializable {

    @SerializedName("before")
    private String _before;

    @SerializedName("after")
    private String _after;

    @SerializedName("ref")
    private String _ref;

    @SerializedName("user_id")
    private int _user_id;

    @SerializedName("user_name")
    private String _user_name;

    // 仓库
    @SerializedName("repository")
    private Repository _repository;

    // 提交，可能有多个commits
    @SerializedName("commits")
    private List<Commit> _commits = new ArrayList<>();

    // 提交的数量
    @SerializedName("total_commits_count")
    private int _total_commits_count;

    public int getTotal_commits_count() {
        return _total_commits_count;
    }
    public void setTotal_commits_count(int total_commits_count) {
        this._total_commits_count = total_commits_count;
    }
    public List<Commit> getCommits() {
        return _commits;
    }
    public void setCommits(List<Commit> commits) {
        this._commits = commits;
    }
    public Repository getRepository() {
        return _repository;
    }
    public void setRepository(Repository repository) {
        this._repository = repository;
    }
    public String getBefore() {
        return _before;
    }
    public void setBefore(String before) {
        this._before = before;
    }
    public String getAfter() {
        return _after;
    }
    public void setAfter(String after) {
        this._after = after;
    }
    public String getRef() {
        return _ref;
    }
    public void setRef(String ref) {
        this._ref = ref;
    }
    public int getUser_id() {
        return _user_id;
    }
    public void setUser_id(int user_id) {
        this._user_id = user_id;
    }
    public String getUser_name() {
        return _user_name;
    }
    public void setUser_name(String user_name) {
        this._user_name = user_name;
    }
}
