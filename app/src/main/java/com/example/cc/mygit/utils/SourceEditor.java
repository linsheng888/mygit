package com.example.cc.mygit.utils;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.os.Build;
import android.text.TextUtils;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.example.cc.mygit.model.CodeDetail;

import java.io.UnsupportedEncodingException;

/**
 * Created by cc on 2017/2/24.
 */

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
@SuppressLint("SetJavaScriptEnabled")
public class SourceEditor {

    private final WebView view;

    private boolean wrap;

    private String name;

    private String content;

    private boolean encoded;

    private boolean markdown;

    /**
     * Create source editor using given web view
     *
     * @param view
     */
    @SuppressLint("AddJavascriptInterface")
    public SourceEditor(final WebView view) {
        WebSettings settings = view.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setBuiltInZoomControls(true);
        try {
            int version = Build.VERSION.SDK_INT;
            if (version >= 11) {
                // 这个方法在API level 11 以上才可以调用，不然会发生异常
                settings.setDisplayZoomControls(false);
            }
        } catch (NumberFormatException e) {

        }
        settings.setUseWideViewPort(true);
        view.addJavascriptInterface(SourceEditor.this, "SourceEditor");
        this.view = view;
    }

    /**
     * @return name
     */
    @JavascriptInterface
    public String getName() {
        return name;
    }

    /**
     * @return content
     */
    @JavascriptInterface
    public String getRawContent() {
        return content;
    }

    /**
     * @return content
     */
    @JavascriptInterface
    public String getContent() {
        if (encoded)
            try {
                return new String(EncodingUtils.fromBase64(content),
                        "UTF-8");
            } catch (UnsupportedEncodingException e) {
                return getRawContent();
            }
        else
            return getRawContent();
    }

    /**
     * @return wrap
     */
    @JavascriptInterface
    public boolean getWrap() {
        return wrap;
    }

    /**
     * @return markdown
     */
    @JavascriptInterface
    public boolean isMarkdown() {
        return markdown;
    }

    /**
     * Set whether lines should wrap
     *
     * @param wrap
     * @return this editor
     */
    @JavascriptInterface
    public SourceEditor setWrap(final boolean wrap) {
        this.wrap = wrap;
        loadSource();
        return this;
    }

    /**
     * Sets whether the content is a markdown file
     *
     * @param markdown
     * @return this editor
     */
    @JavascriptInterface
    public SourceEditor setMarkdown(final boolean markdown) {
        this.markdown = markdown;
        return this;
    }

    /**
     * Bind content to current {@link WebView}
     *
     * @param name
     * @param content
     * @param encoded
     * @return this editor
     */
    @JavascriptInterface
    public SourceEditor setSource(final String name, final String content,
                                  final boolean encoded) {
        this.name = name;
        this.content = content;
        this.encoded = encoded;
        loadSource();

        return this;
    }

    @JavascriptInterface
    private void loadSource() {
        if (name != null && content != null) {
            view.loadUrl("file:///android_asset/source-editor.html");
        }
    }

    /**
     * Bind blob content to current {@link WebView}
     *
     * @param name
     * @param blob
     * @return this editor
     */
    @JavascriptInterface
    public SourceEditor setSource(final String name, final CodeDetail blob) {

        String content = blob.getContent();
        if (content == null)
            content = "";
        boolean encoded = !TextUtils.isEmpty(content)
                && CodeDetail.ENCODING_BASE64.equals(blob.getEncoding());
        return setSource(name, content, encoded);
    }

    /**
     * Toggle line wrap
     *
     * @return this editor
     */
    @JavascriptInterface
    public SourceEditor toggleWrap() {
        return setWrap(!wrap);
    }

    /**
     * Toggle markdown file rendering
     *
     * @return this editor
     */
    @JavascriptInterface
    public SourceEditor toggleMarkdown() {
        return setMarkdown(!markdown);
    }
}
