package com.example.cc.mygit.view.detail;

import com.example.cc.mygit.GitApplication;
import com.example.cc.mygit.R;
import com.example.cc.mygit.model.Count;
import com.example.cc.mygit.model.Project;
import com.example.cc.mygit.model.User;
import com.example.cc.mygit.source.API;
import com.haibin.elegant.Response;
import com.haibin.elegant.call.CallBack;

/**
 * Created by cc on 2016/11/13.
 */

public class ProjectDetailPresenter implements ProjectDetailContract.Presenter {
    private final ProjectDetailContract.View mView;
    private final ProjectDetailContract.ActionView mActionView;
    private Project mProject;
    private boolean isStarting, isWatching;
    private User mUser;


    public ProjectDetailPresenter(ProjectDetailContract.View mView,
                                  ProjectDetailContract.ActionView actionView,
                                  Project project) {
        this.mView = mView;
        this.mActionView = actionView;
        this.mProject = project;
        mUser = GitApplication.getConfig().getUser();
        this.mView.setPresenter(this);
    }

    @Override

    public void getProjectDetail(long id) {
        API.getProjectDetail(id, mUser.getPrivateToken(), new CallBack<Project>() {
            @Override
            public void onResponse(Response<Project> response) {
                Project project = response.getBody();
                if (project != null) {
                    mProject = project;
                    mView.onGetDetailSuccess(project);
                    if (project.isWatched())
                        mActionView.onWatchSuccess(project.getWatchesCount());
                    if (project.isStared())
                        mActionView.onStarSuccess(project.getStarsCount());
                }
            }

            @Override
            public void onFailure(Exception e) {
                mView.showNetworkError(R.string.network_error);
            }
        });

    }

    @Override
    public void getProjectDetail(String namespace) {
        API.getProjectDetail(namespace, mUser.getPrivateToken(), new CallBack<Project>() {
            @Override
            public void onResponse(Response<Project> response) {
                Project project = response.getBody();
                if (project != null) {
                    mView.onGetDetailSuccess(project);
                }
            }

            @Override
            public void onFailure(Exception e) {
                mView.showNetworkError(R.string.network_error);
            }
        });

    }

    @Override
    public void startOrUnStart() {
        if (isStarting)
            return;
        isStarting = true;
        String namespace = mProject.getPathWithNamespace().replace("/", "%2F").replace(".", "+");
        if (mProject.isStared()) {
            unStart(namespace);
        } else {
            start(namespace);
        }

    }

    @Override
    public void watchOrUnwatch() {
        if (isWatching)
            return;
        isWatching = true;
        String namespace = mProject.getPathWithNamespace().replace("/", "%2F").replace(".", "+");
        if (mProject.isWatched()) {
            unwatch(namespace);
        } else {
            watch(namespace);
        }

    }

    private void start(String namespace) {
        API.startProject(namespace,
                GitApplication.getConfig().getUser().getPrivateToken(),
                new CallBack<Count>() {
                    @Override
                    public void onResponse(Response<Count> response) {
                        Count count = response.getBody();
                        if (count != null) {
                            mActionView.onStarSuccess(count.getCount());
                            mProject.setStared(true);
                            mProject.setStarsCount(count.getCount());
                        } else {
                            mActionView.onStartFailure(R.string.start);
                        }
                        isStarting = false;
                    }

                    @Override
                    public void onFailure(Exception e) {
                        isStarting = false;
                        mView.showNetworkError(R.string.network_error);

                    }
                });
    }

    private void unStart(String namespace) {
        API.unstartProject(namespace,
                GitApplication.getConfig().getUser().getPrivateToken(),
                new CallBack<Count>() {
                    @Override
                    public void onResponse(Response<Count> response) {
                        Count count = response.getBody();
                        if (count != null) {
                            mProject.setStared(false);
                            mProject.setStarsCount(count.getCount());
                            mActionView.onUnStartSuccess(count.getCount());
                        } else {
                            mActionView.onUnStartFailure(R.string.start);
                        }
                        isStarting = false;

                    }

                    @Override
                    public void onFailure(Exception e) {
                        isStarting = false;
                        mView.showNetworkError(R.string.network_error);

                    }
                });

    }

    private void watch(String namespace) {
        API.watchProject(namespace,
                GitApplication.getConfig().getUser().getPrivateToken(),
                new CallBack<Count>() {
                    @Override
                    public void onResponse(Response<Count> response) {
                        Count count = response.getBody();
                        if (count != null) {
                            mProject.setWatched(true);
                            mProject.setWatchesCount(count.getCount());
                            mActionView.onWatchSuccess(count.getCount());
                        } else {
                            mActionView.onWatchFailure(R.string.start);
                        }
                        isWatching = false;
                    }

                    @Override
                    public void onFailure(Exception e) {
                        isWatching = false;
                        mView.showNetworkError(R.string.network_error);
                    }
                });


    }

    private void unwatch(String namespace) {
        API.unwatchProject(namespace,
                GitApplication.getConfig().getUser().getPrivateToken(),
                new CallBack<Count>() {
                    @Override
                    public void onResponse(Response<Count> response) {
                        Count count = response.getBody();
                        if (count != null) {
                            mProject.setWatched(false);
                            mProject.setWatchesCount(count.getCount());
                            mActionView.onUnwatchSuccess(count.getCount());
                        } else {
                            mActionView.onUnwatchFailure(R.string.start);
                        }
                        isWatching = false;
                    }

                    @Override
                    public void onFailure(Exception e) {
                        isWatching = false;
                        mView.showNetworkError(R.string.network_error);
                    }
                });
    }

    @Override
    public void start() {

    }
}
