package com.example.cc.mygit.view.detail;

import com.example.cc.mygit.model.Count;
import com.example.cc.mygit.model.Project;
import com.haibin.elegant.call.Call;
import com.haibin.elegant.net.Form;
import com.haibin.elegant.net.GET;
import com.haibin.elegant.net.POST;
import com.haibin.elegant.net.Path;

/**
 * Created by cc on 2016/11/13.
 */

public interface ProjectDetailService {
    /**
     * 获取项目详情
     */
    @GET("projects/{id}")
    Call<Project> getProjectDetail(@Path("id") long id,
                                   @Form("private_token") String token);

    /**
     * 获取项目详情
     */
    @GET("projects/{namespace}")
    Call<Project> getProjectDetail(@Path("namespace") String namespace,
                                   @Form("private_token") String token);


    /**
     * start项目
     */
    @POST("projects/{project_path_with_namespace}/star")
    Call<Count> start(@Path("project_path_with_namespace") String project_path_with_namespace,
                      @Form("project_path_with_namespace") String namespace,
                      @Form("private_token") String token);

    /**
     * unstart项目
     */
    @POST("projects/{project_path_with_namespace}/unstar")
    Call<Count> unstart(@Path("project_path_with_namespace") String project_path_with_namespace,
                        @Form("project_path_with_namespace") String namespace,
                        @Form("private_token") String token);


    /**
     * watch
     */
    @POST("projects/{project_path_with_namespace}/watch")
    Call<Count> watch(@Path("project_path_with_namespace") String project_path_with_namespace,
                      @Form("project_path_with_namespace") String namespace,
                      @Form("private_token") String token);

    /**
     * unwatch项目
     */
    @POST("projects/{project_path_with_namespace}/unwatch")
    Call<Count> unwatch(@Path("project_path_with_namespace") String project_path_with_namespace,
                        @Form("project_path_with_namespace") String namespace,
                        @Form("private_token") String token);

}

