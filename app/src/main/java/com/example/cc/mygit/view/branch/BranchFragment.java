package com.example.cc.mygit.view.branch;

import android.os.Bundle;

import com.example.cc.mygit.R;
import com.example.cc.mygit.base.BaseRecyclerFragment;
import com.example.cc.mygit.model.Branch;
import com.example.cc.mygit.model.Project;
import com.haibin.androidbase.adapter.BaseRecyclerAdapter;

/**
 * Created by cc on 2017/2/22.
 */

public class BranchFragment extends
        BaseRecyclerFragment<BranchContract.Presenter, Branch> implements
        BranchContract.View {

    public static BranchFragment newInstance(Project project) {
        BranchFragment fragment = new BranchFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("project", project);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_branch;
    }

    @Override
    protected void initData() {
        new BranchPresenter(this, (Project)getArguments().getSerializable("project")).start();//初始化后如何传递到setPresenter
        super.initData();
    }

    @Override
    public void onItemClick(int position, long itemId) {

    }

    @Override
    protected BaseRecyclerAdapter<Branch> getAdapter() {
        return null;
    }
}
