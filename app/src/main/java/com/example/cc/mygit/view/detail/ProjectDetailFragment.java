package com.example.cc.mygit.view.detail;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.TextView;

import com.example.cc.mygit.R;
import com.example.cc.mygit.model.Project;
import com.example.cc.mygit.model.User;
import com.example.cc.mygit.utils.ImageLoader;
import com.example.cc.mygit.view.readme.ReadmeActivity;
import com.example.cc.mygit.view.userInfo.UserInfoActivity;
import com.haibin.androidbase.fragment.BaseFragment;
import com.haibin.androidbase.utils.DateTimeFormat;
import com.haibin.androidbase.widget.CircleImageView;

import butterknife.Bind;
import butterknife.OnClick;

import static android.R.attr.onClick;
import static com.example.cc.mygit.R.string.project;

/**
 * Created by cc on 2016/12/13.
 */

public class ProjectDetailFragment extends BaseFragment implements ProjectDetailContract.View, View.OnClickListener{

    @Bind(R.id.tv_project_name)
    TextView tv_project_name;

    @Bind(R.id.tv_time)
    TextView tv_time;

    @Bind(R.id.tv_title)
    TextView tv_title;

    @Bind(R.id.tv_description)
    TextView tv_description;

    @Bind(R.id.civ_author)
    CircleImageView mImageAuthor;

    private ProjectDetailPresenter mPresenter;
    private Project mProject;
    private User mUser;

    public static ProjectDetailFragment newInstance(Project project) {
        ProjectDetailFragment fragment = new ProjectDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("project",project);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_project_detail;
    }

    @Override
    protected void initData() {
        super.initData();
        mProject = (Project) getArguments().getSerializable("project");
        tv_project_name.setText(mProject.getName());
        tv_time.setText(DateTimeFormat.format(mProject.getCreatedTime()));
        tv_description.setText(mProject.getDescription());
        tv_title.setText(mProject.getDescription());
        ImageLoader.getGlide()
                .load(mProject.getOwner().getNewPortrait())
                .asBitmap()
                .placeholder(R.mipmap.icon_git)
                .into(mImageAuthor);

        mUser = new User();
    }

    @OnClick({R.id.btn_readme,R.id.civ_author})
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
                case R.id.btn_readme:
                    ReadmeActivity.show(mContext, mProject);
                    break;

                case R.id.civ_author:
                    UserInfoActivity.show(mContext,mProject);
                    break;
        }
    }

    @Override
    public void onGetDetailSuccess(Project project) {

    }

    @Override
    public void showGetDetailFailure(int strId) {
        showToastLong(strId);

    }

    @Override
    public void showNetworkError(int strId) {
        showToastShort(strId);

    }

    @Override
    public void setPresenter(ProjectDetailContract.Presenter presenter) {

        this.mPresenter = (ProjectDetailPresenter) presenter;
    }
}
