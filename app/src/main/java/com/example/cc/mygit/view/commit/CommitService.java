package com.example.cc.mygit.view.commit;

import com.example.cc.mygit.model.Commit;
import com.haibin.elegant.call.Call;
import com.haibin.elegant.net.Form;
import com.haibin.elegant.net.GET;
import com.haibin.elegant.net.Path;

import java.util.List;

/**
 * Created by cc on 2017/1/1.
 */

public interface CommitService {
    /**
     * 提交列表
     */
    @GET("projects/{project_id}/repository/commits")
    Call<List<Commit>> getCommits(@Path("project_id") long projectId,
                                  @Form("private_token") String token,
                                  @Form("page") int page);
}
