package com.example.cc.mygit.view.code.detail;

import com.example.cc.mygit.R;
import com.example.cc.mygit.model.CodeDetail;
import com.example.cc.mygit.source.API;
import com.haibin.elegant.Response;
import com.haibin.elegant.call.CallBack;

/**
 * Created by cc on 2017/2/23.
 */

public class CodeDetailPresenter  implements CodeDetailContract.Presenter{


    private final CodeDetailContract.View mView;
    private long mProjectId;
    private String mPath;
    private String mRef;

    public CodeDetailPresenter(CodeDetailContract.View mView, long mProjectId, String mPath, String mRef) {
        this.mView = mView;
        this.mProjectId = mProjectId;
        this.mPath = mPath;
        this.mRef = mRef;
        this.mView.setPresenter(this);
    }

    @Override
    public void start() {

    }

    @Override
    public void getCodeDetail() {
        API.getCodeDetail(mProjectId,
                mPath,
                mRef,
                new CallBack<CodeDetail>() {
                    @Override
                    public void onResponse(Response<CodeDetail> response) {
                        CodeDetail detail = response.getBody();
                        if (detail != null){
                            mView.showGetCodeDetailSuccess(mPath,detail);
                        }else{
                            mView.showGetCodeDetailError(R.string.code_detail);
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {

                    }
                });
    }
}
