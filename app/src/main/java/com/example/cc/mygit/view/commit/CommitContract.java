package com.example.cc.mygit.view.commit;

import com.example.cc.mygit.model.Commit;
import com.example.cc.mygit.present.BaseListPresenter;
import com.example.cc.mygit.present.BaseListView;

/**
 * Created by cc on 2017/1/1.
 */

public interface CommitContract {

    interface View extends BaseListView<Presenter, Commit> {

    }

    interface Presenter extends BaseListPresenter {

    }
}
