package com.example.cc.mygit.view.code;

import com.example.cc.mygit.model.Code;
import com.example.cc.mygit.model.Project;
import com.example.cc.mygit.model.User;
import com.example.cc.mygit.source.API;
import com.haibin.elegant.Response;
import com.haibin.elegant.call.CallBack;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by cc on 2017/2/23.
 */

public class CodePresenter implements CodeContract.Presenter {

    private LinkedHashMap<Integer,List<Code>> mCodeMap;
    private final CodeContract.View mView;
    private final CodeContract.ActionView mActionView;

    private Project mProject;
    private String mBranch;
    private User mUser;
    private List<String> mPaths;
    boolean isLoading = false;

    public CodePresenter(CodeContract.View mView, CodeContract.ActionView mActionView, Project mProject, User mUser) {
        this.mView = mView;
        this.mActionView = mActionView;
        this.mProject = mProject;
        this.mUser = mUser;
        this.mView.setPresenter(this);
    }

    @Override
    public void start() {

        mBranch = "master";
        mPaths = new ArrayList<>();
        mCodeMap = new LinkedHashMap<>();
    }

    @Override
    public void onRefreshing() {

        isLoading = true;
        API.getCodeRepository(mProject.getId(),
                getPath(),
                mBranch,
                mUser.getPrivateToken(),
                new CallBack<List<Code>>() {
                    @Override
                    public void onResponse(Response<List<Code>> response) {
                        List<Code> codes = response.getBody();
                        if (codes != null) {
                            if(mPaths.size()== 0){
                                mCodeMap.put(0, codes);
                            }
                            mView.onRefreshSuccess(codes);
                        } else {
                            mView.onRefreshFailure();
                        }
                        mView.onComplete();
                        isLoading = false;
                    }

                    @Override
                    public void onFailure(Exception e) {
                        mView.showNetWorkError();
                        mView.onComplete();
                        isLoading = false;
                    }
                });
    }

    @Override
    public void onLoadMore() {

    }

    @Override
    public void remove(int position) {

        if (mCodeMap.size() < position && mCodeMap.containsKey(position))
            return;
        int size = mCodeMap.size();
        for (int i = position; i <= size; i++) {
            mCodeMap.remove(i + 1);
            if (mPaths.size() > position && position >= 0)
                mPaths.remove(position);//一直移除最后一项
        }
        isLoading = false;
    }

    @Override
    public void preLoad(int position) {
        isLoading = true;
        if (mPaths.size() < position)
            return;
        List<Code> codes = mCodeMap.get(position);
        mView.onPreLoadSuccess(codes);
        remove(position);
    }

    @Override
    public void load(final String path) {

        isLoading = true;
        API.getCodeRepository(mProject.getId(),
                getPath() + path + "/",
                mBranch,
                mUser.getPrivateToken(),
                new CallBack<List<Code>>() {
                    @Override
                    public void onResponse(Response<List<Code>> response) {
                        List<Code> codes = response.getBody();
                        if (codes != null && codes.size() != 0){
                            mCodeMap.put(mPaths.size()+1,codes);
                            mPaths.add(path);
                            mActionView.onAddPath(path);
                            mView.onRefreshSuccess(codes);
                        }else{
                            mView.onRefreshFailure();
                        }
                        mView.onComplete();
                        isLoading = false;

                    }

                    @Override
                    public void onFailure(Exception e) {
                        mView.showNetWorkError();
                        mView.onComplete();
                        isLoading = false;
                    }
                });
    }

    public String getPath() {
        StringBuilder sb = new StringBuilder();
        for (String s : mPaths){
            sb.append(s+"/");
        }
        return sb.toString();
    }
    boolean isCanBack(){
        return mPaths.size() == 0;
    }
}
