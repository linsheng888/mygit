package com.example.cc.mygit.source;

import com.example.cc.mygit.model.Project;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by cc on 2017/5/2.
 */

public interface RetrofitService {

    /**
     * 获取项目列表，featured 推荐 -- popular 热门  --  latest 最近更新
     */
    @GET("projects/{path}")
    Call<List<Project>> getProjectList(@Path("path") String path,
                                       @Query("page") int page,
                                       @Query("private_token") String token);

    @GET("projects/{path}")
    Call<List<Project>> getProject(@Path("path") String path,
                                       @Query("page") int page,
                                       @Query("private_token") String token);

}