package com.example.cc.mygit.view.language.project;

import com.example.cc.mygit.model.Project;
import com.example.cc.mygit.present.BaseListPresenter;
import com.example.cc.mygit.present.BaseListView;

/**
 * * 语言底下的项目列表
 * Created by cc on 2016/11/13.
 */

public interface LanguageProjectContract {

    interface View extends BaseListView<Presenter,Project>{

    }

    interface Presenter extends BaseListPresenter{

    }

}
