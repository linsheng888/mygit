package com.example.cc.mygit.view.commit;

import android.content.Context;

import com.example.cc.mygit.R;
import com.example.cc.mygit.model.Commit;
import com.example.cc.mygit.model.User;
import com.example.cc.mygit.utils.ImageLoader;
import com.haibin.androidbase.adapter.BaseRecyclerAdapter;
import com.haibin.androidbase.adapter.ViewHolder;
import com.haibin.androidbase.utils.DateTimeFormat;

/**
 * Created by cc on 2017/1/1.
 */

public class CommitAdapter extends BaseRecyclerAdapter<Commit> {

    public CommitAdapter(Context context) {
        super(context, ONLY_FOOTER);
    }

    @Override
    protected int getLayoutId(int viewType) {
        return R.layout.item_list_commit;
    }

    @Override
    protected void onBindDefaultViewHolder(ViewHolder holder, Commit item, int position) {
        holder.bindText(R.id.tv_author, item.getAuthorName());
        holder.bindText(R.id.tv_commit, item.getTitle());
        holder.bindText(R.id.tv_time, DateTimeFormat.format(item.getCreatedAt()));
        User user = item.getAuthor();
        if (user != null)
            ImageLoader.getGlide().load(user.getNewPortrait())
                    .asBitmap()
                    .placeholder(R.mipmap.icon_git)
                    .into(holder.getImageView(R.id.civ_author));
    }
}
