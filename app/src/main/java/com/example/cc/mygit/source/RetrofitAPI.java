package com.example.cc.mygit.source;

import com.example.cc.mygit.model.Project;

import java.util.List;

import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by cc on 2017/5/2.
 */

public class RetrofitAPI {

    public static final Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("http://git.oschina.net/api/v3/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    /**
     * 获取项目列表，featured 推荐 -- popular 热门  --  latest 最近更新
     */
    public static void getProjects(String type, int page, String token, Callback<List<Project>> callback){

        retrofit.create(RetrofitService.class)
                .getProjectList(type,page,token).enqueue(callback);
    }

}
