package com.example.cc.mygit.source;

import android.content.Context;

import com.example.cc.mygit.model.User;
import com.google.gson.Gson;
import com.haibin.androidbase.utils.DataConfig;

/**
 * Created by cc on 2016/11/13.
 */

public class GitAppConfig extends DataConfig {
    public GitAppConfig(Context context, String fileName) {
        super(context, fileName);
    }

    public void putUser(User user) {
        put("user", new Gson().toJson(user));
    }

    //清除用户信息
    public void logout() {
        put("user", "");
    }

    public User getUser() {
        try {
            return new Gson().fromJson(getString("user", ""), User.class);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
