package com.example.cc.mygit.view.userInfo.userStartProjects;

import com.example.cc.mygit.model.Project;
import com.haibin.elegant.call.Call;
import com.haibin.elegant.net.Form;
import com.haibin.elegant.net.GET;
import com.haibin.elegant.net.Path;

import java.util.List;

/**
 * Created by cc
 * on 2017/3/18.
 */

public interface UserStartProjectsService {

    /**
     * 获取用户Start项目列表
     */
    @GET("user/{uid}/stared_projects")
    Call<List<Project>> getUserStartProjectsList(@Path("uid") String uid,
                                            @Form("page") int page,
                                            @Form("private_token") String token);
}
