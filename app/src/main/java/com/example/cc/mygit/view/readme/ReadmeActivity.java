package com.example.cc.mygit.view.readme;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.graphics.drawable.DrawableCompat;

import com.example.cc.mygit.R;
import com.example.cc.mygit.model.Project;
import com.example.cc.mygit.utils.IO;
import com.haibin.androidbase.activity.BaseBackActivity;

import java.io.IOException;

/**
 * Created by cc on 2016/12/13.
 */

public class ReadmeActivity extends BaseBackActivity{

    private ReadmePresenter mPresenter;
    private ReadmeFragment mFragment;
    private Project mProject;

    public static void show(Context context, Project project) {
        Intent intent = new Intent(context, ReadmeActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("project", project);
        intent.putExtras(bundle);
        context.startActivity(intent);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_readme;
    }

    @Override
    protected void initView() {
        super.initView();
        mToolBar.setNavigationIcon(R.mipmap.icon_close);
        DrawableCompat.setTint(mToolBar.getNavigationIcon(),0xFFFFFFFF);
    }

    @Override
    protected void initData() {
        super.initData();
        mProject = (Project) getIntent().getExtras().getSerializable("project");
        String html = null;
        try {
            html = IO.asString(getAssets().open("README.HTML"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        mFragment = ReadmeFragment.newInstance(mProject);
        addFragment(R.id.fl_content,mFragment);
        mPresenter = new ReadmePresenter(mFragment,html);
        mPresenter.start();
        mPresenter.getReadme(mProject.getPathWithNamespace());
    }
}
