package com.example.cc.mygit.view.setting;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.cc.mygit.GitApplication;
import com.example.cc.mygit.R;
import com.example.cc.mygit.model.Follow;
import com.example.cc.mygit.model.Project;
import com.example.cc.mygit.model.User;
import com.example.cc.mygit.utils.ImageLoader;
import com.example.cc.mygit.view.login.LoginActivity;
import com.example.cc.mygit.view.readme.ReadmeActivity;
import com.haibin.androidbase.activity.BaseBackActivity;
import com.haibin.androidbase.widget.CircleImageView;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by cc on 2017/3/19.
 */

public class MyInfoActivity extends BaseBackActivity implements View.OnClickListener {

    @Bind(R.id.civ_author)
    CircleImageView mImageAuthor;

    @Bind(R.id.tv_user_name)
    TextView userName;

    @Bind(R.id.tv_jointime)
    TextView joinTime;

    @Bind(R.id.tv_description)
    TextView userDescription;

    @Bind(R.id.tv_followers)
    TextView followers;

    @Bind(R.id.tv_stared)
    TextView stared;

    @Bind(R.id.tv_following)
    TextView following;

    @Bind(R.id.tv_watched)
    TextView watched;

    @Bind(R.id.btn_logout)
    Button logout;

    private User mUser;

    public static void show(Fragment fragment, User user,int requestCode) {
        Intent intent = new Intent(fragment.getActivity(), MyInfoActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("user", user);
        intent.putExtras(bundle);
        fragment.startActivityForResult(intent,requestCode);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_myinfo_detail;
    }

    @Override
    protected void initView() {
        super.initView();
        mToolBar.setNavigationIcon(R.mipmap.icon_close);
        DrawableCompat.setTint(mToolBar.getNavigationIcon(), 0xFFFFFFFF);

        mUser = (User) getIntent().getExtras().getSerializable("user");

    }

    @Override
    protected void initData() {
        super.initData();

        ImageLoader.getGlide()
                .load(mUser.getNewPortrait())
                .asBitmap()
                .placeholder(R.mipmap.icon_git)
                .into(mImageAuthor);

        userName.setText(mUser.getName());
        joinTime.setText(mUser.getCreatedData().substring(0, 10));
        if (TextUtils.isEmpty(mUser.getBio())) {
            userDescription.setText("暂无填写");
        } else {
            userDescription.setText(mUser.getBio());
        }

        Follow follow = mUser.getFollow();
        if (follow == null)
            follow = new Follow();
        followers.setText(String.valueOf(follow.getFollowers()));
        following.setText(String.valueOf(follow.getFollowing()));
        stared.setText(String.valueOf(follow.getStarred()));
        watched.setText(String.valueOf(follow.getWatched()));

    }

    @OnClick({R.id.btn_logout})
    @Override
    public void onClick(View v) {
        GitApplication.getConfig().logout();
        LoginActivity.show(this);
        setResult(RESULT_OK,null);
        finish();

    }
}
