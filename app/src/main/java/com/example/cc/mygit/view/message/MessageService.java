package com.example.cc.mygit.view.message;

import com.example.cc.mygit.model.Message;
import com.haibin.elegant.call.Call;
import com.haibin.elegant.net.Form;
import com.haibin.elegant.net.GET;

import java.util.List;

/**
 * Created by cc on 2016/11/13.
 */

public interface MessageService {
    /**
     * 获取消息中心信息
     *
     * @param token 登陆用户的私有token
     * @param all   指定通知的类型 0:未读 1已读
     */
    @GET("user/notifications")
    Call<List<Message>> getMessages(@Form("private_token") String token,
                                    @Form("filter") String filter,
                                    @Form("all") String all);

}
