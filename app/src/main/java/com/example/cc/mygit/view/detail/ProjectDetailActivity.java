package com.example.cc.mygit.view.detail;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.cc.mygit.R;
import com.example.cc.mygit.model.Project;
import com.example.cc.mygit.view.code.CodeActivity;
import com.example.cc.mygit.view.commit.CommitActivity;
import com.example.cc.mygit.view.issue.IssueActivity;
import com.haibin.androidbase.activity.BaseBackActivity;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by cc on 2016/11/13.
 */

public class ProjectDetailActivity extends BaseBackActivity implements View.OnClickListener,
        ProjectDetailContract.ActionView{

    private ProjectDetailPresenter mPresenter;
    private ProjectDetailFragment mFragment;

    @Bind(R.id.iv_start)
    ImageView mImageStart;

    @Bind(R.id.tv_start)
    TextView mTextStart;

    @Bind(R.id.iv_watch)
    ImageView mImageWatch;

    @Bind(R.id.tv_watch)
    TextView mTextWatch;

    private Project mProject;

    public static void show(Fragment fragment, Project project,int requestCode){
        Intent intent = new Intent(fragment.getActivity(),ProjectDetailActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("project",project);
        bundle.putInt("code",requestCode);
        intent.putExtras(bundle);
        fragment.startActivityForResult(intent,requestCode);

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_project_detail;
    }

    @Override
    protected void initData() {
        super.initData();
        mProject = (Project) getIntent().getExtras().getSerializable("project");
        mFragment = ProjectDetailFragment.newInstance(mProject);
        addFragment(R.id.fl_content, mFragment);
        mPresenter = new ProjectDetailPresenter(mFragment, this, mProject);
        mPresenter.start();
        mPresenter.getProjectDetail(mProject.getId());
        setTitle(mProject.getOwner().getName());
        mImageStart.setImageResource(mProject.isStared() ? R.mipmap.icon_start : R.mipmap.icon_unstart);
        mTextStart.setText(String.valueOf(mProject.getStarsCount()));
        mImageWatch.setImageResource(mProject.isWatched() ? R.mipmap.icon_watch : R.mipmap.icon_unwatch);
        mTextWatch.setText(String.valueOf(mProject.getWatchesCount()));

    }

    @OnClick({R.id.ll_start, R.id.ll_watch, R.id.ll_question, R.id.ll_code,R.id.btn_commit})
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_start:
                mPresenter.startOrUnStart();
                break;
            case R.id.ll_watch:
                mPresenter.watchOrUnwatch();
                break;
            case R.id.ll_question:
                IssueActivity.show(this,mProject);
                break;
            case R.id.ll_code:
                CodeActivity.show(this, mProject);
                break;
            case R.id.btn_commit:
                CommitActivity.show(this,mProject);
                break;
        }

    }

    @Override
    public void onStarSuccess(int count) {
        mImageStart.setImageResource(R.mipmap.icon_start);
        mTextStart.setText(String.valueOf(count));
    }

    @Override
    public void onStartFailure(int strId) {
        showToastShort(strId);
    }

    @Override
    public void onUnStartSuccess(int count) {
        mImageStart.setImageResource(R.mipmap.icon_unstart);
        mTextStart.setText(String.valueOf(count));
    }

    @Override
    public void onUnStartFailure(int strId) {
        showToastShort(strId);
    }

    @Override
    public void onWatchSuccess(int count) {
        mImageWatch.setImageResource(R.mipmap.icon_watch);
        mTextWatch.setText(String.valueOf(count));
    }

    @Override
    public void onWatchFailure(int strId) {
        showToastShort(strId);
    }

    @Override
    public void onUnwatchSuccess(int count) {
        mImageWatch.setImageResource(R.mipmap.icon_unwatch);
        mTextWatch.setText(String.valueOf(count));
    }

    @Override
    public void onUnwatchFailure(int strId) {
        showToastShort(strId);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        supportFinishAfterTransition();
    }
}
