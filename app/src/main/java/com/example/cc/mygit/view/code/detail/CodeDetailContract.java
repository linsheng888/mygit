package com.example.cc.mygit.view.code.detail;

import com.example.cc.mygit.model.CodeDetail;
import com.example.cc.mygit.present.BasePresenter;
import com.example.cc.mygit.present.BaseView;

/**
 * Created by cc on 2017/1/25.
 */

public interface CodeDetailContract {

    interface View extends BaseView<Presenter> {
        void showGetCodeDetailSuccess(String url, CodeDetail detail);

        void showGetCodeDetailError(int strId);

        void showNetworkError(int strId);

    }

    interface Presenter extends BasePresenter {
        void getCodeDetail();
    }
}
