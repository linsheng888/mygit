package com.example.cc.mygit.present;

import java.util.List;

/**
 * Created by cc on 2016/11/12.
 */

public interface BaseListView<T extends BaseListPresenter, M > extends BaseView<T>{

    void onRefreshSuccess(List<M> items);

    void onLoadMoreSuccess(List<M> items);

    void showNetWorkError();

    void onComplete();

    void onRefreshFailure();

    void showNoMoreData();

}
