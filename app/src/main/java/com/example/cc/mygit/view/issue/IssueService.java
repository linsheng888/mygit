package com.example.cc.mygit.view.issue;

import com.example.cc.mygit.model.Issue;
import com.haibin.elegant.call.Call;
import com.haibin.elegant.net.Form;
import com.haibin.elegant.net.GET;
import com.haibin.elegant.net.Path;

import java.util.List;

/**
 * Created by cc on 2017/1/1.
 */

public interface IssueService {

    @GET("projects/{project_id}/issues")
    Call<List<Issue>> getIssues(@Path("project_id") long project_id,
                                @Form("private_token") String token,
                                @Form("page") int page);

    @GET("projects/{project_owner_username}/{project_name}/issues/{issue_id}")
    Call<Issue> getIssueDetail(@Path("project_owner_username") String project_owner_username,
                               @Path("project_name") String project_name,
                               @Path("issue_id") long issueId,
                               @Form("private_token") String token);
}
