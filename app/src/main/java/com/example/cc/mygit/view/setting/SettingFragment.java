package com.example.cc.mygit.view.setting;

import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.cc.mygit.GitApplication;
import com.example.cc.mygit.R;
import com.example.cc.mygit.model.Follow;
import com.example.cc.mygit.model.Project;
import com.example.cc.mygit.model.User;
import com.example.cc.mygit.utils.ImageLoader;
import com.example.cc.mygit.view.readme.ReadmeActivity;
import com.example.cc.mygit.view.userInfo.UserInfoViewPageFragment;
import com.haibin.androidbase.fragment.BaseFragment;
import com.haibin.androidbase.widget.CircleImageView;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by cc on 2016/11/13.
 */

public class SettingFragment extends BaseFragment implements View.OnClickListener {

    @Bind(R.id.civ_author)
    CircleImageView mImageAuthor;

    @Bind(R.id.toolBar)
    Toolbar mToolBar;

    @Bind(R.id.tv_user_name)
    TextView userName;

    @Bind(R.id.tv_signature)
    TextView mTextSifnature;

    @Bind(R.id.coll_toolbal)
    CollapsingToolbarLayout mCollToolbar;


    private User mUser;
    private Project mProject;

    public static SettingFragment newInstance() {

        return new SettingFragment();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_setting;
    }

    @Override
    protected void initView() {
        super.initView();
        mUser = GitApplication.getConfig().getUser();

        mCollToolbar.setTitle(mUser.getUsername());
        mCollToolbar.setCollapsedTitleTextColor(Color.WHITE);
        mCollToolbar.setExpandedTitleColor(Color.TRANSPARENT);

        ImageLoader.getGlide()
                .load(mUser.getNewPortrait())
                .asBitmap()
                .placeholder(R.mipmap.icon_git)
                .into(mImageAuthor);

        mToolBar.setTitle(mUser.getUsername());
        mToolBar.inflateMenu(R.menu.menu_setting);
        mToolBar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.menu_edit:
                        //
                        MyInfoActivity.show(SettingFragment.this, mUser, 1);
                        break;
                    case R.id.menu_share:
                        //
                        break;
                }
                return false;
            }
        });

//        Follow follow = mUser.getFollow();
//        if (follow == null)
//            follow = new Follow();
//        mTextFollowers.setText(String.format("followers: %s", follow.getFollowers()));
//        mTextFollowing.setText(String.format("following: %s", follow.getFollowing()));

        userName.setText(mUser.getName());
        mTextSifnature.setText(TextUtils.isEmpty(mUser.getBio()) ? getResourceString(R.string.not_description) : mUser.getBio());

        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.add(R.id.fl_content, UserInfoViewPageFragment.newInstance(mUser));
        transaction.commit();
    }

    @OnClick({R.id.civ_author})
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.civ_author:
//                MyInfoActivity.show(this, mUser, 1);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != AppCompatActivity.RESULT_OK)
            return;
        switch (requestCode) {
            case 1:
                getActivity().finish();
                break;
            case 2:
                //修改其他信息
                MyInfoActivity.show(this, mUser, 1);
                break;
        }
    }

    //    public void addFragment(int frameLayoutId, Fragment fragment) {
//        if (fragment != null) {
//            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//            if (fragment.isAdded()) {
//                if (mFragment != null) {
//                    transaction.hide(mFragment).show(fragment);
//                } else {
//                    transaction.show(mFragment);
//                }
//            } else {
//                if (mFragment != null) {
//                    transaction.hide(mFragment).add(frameLayoutId, fragment);
//                } else {
//                    transaction.add(frameLayoutId, fragment);
//                }
//            }
//            mFragment = fragment;
//            transaction.commit();
//        }
//    }
}
