package com.example.cc.mygit.view.userInfo;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.cc.mygit.R;
import com.example.cc.mygit.model.Follow;
import com.example.cc.mygit.model.Project;
import com.example.cc.mygit.model.User;
import com.example.cc.mygit.utils.ImageLoader;
import com.haibin.androidbase.activity.BaseBackActivity;
import com.haibin.androidbase.widget.CircleImageView;

import butterknife.Bind;

/**
 * Created by cc
 * on 2017/3/18.
 */

public class UserInfoActivity extends BaseBackActivity {

    @Bind(R.id.civ_author)
    CircleImageView mImageAuthor;

    @Bind(R.id.toolBar)
    Toolbar mToolBar;

    @Bind(R.id.tv_user_name)
    TextView userName;

    @Bind(R.id.tv_signature)
    TextView mTextSifnature;

    private Project mProject;
    private User mUser;
    private CollapsingToolbarLayout mCollToolbar;

    public static void show(Context context, Project project) {
        Intent intent = new Intent(context, UserInfoActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("project", project);
        intent.putExtras(bundle);
        context.startActivity(intent);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_user_info;
    }

    @Override
    protected void initView() {
        super.initView();
        mProject = (Project) getIntent().getExtras().getSerializable("project");
        mUser = mProject.getOwner();

        mToolBar.setNavigationIcon(R.mipmap.icon_close);
        DrawableCompat.setTint(mToolBar.getNavigationIcon(), 0xFFFFFFFF);
        mCollToolbar = (CollapsingToolbarLayout) findViewById(R.id.coll_toolbal);

        ImageLoader.getGlide()
                .load(mUser.getNewPortrait())
                .asBitmap()
                .placeholder(R.mipmap.icon_git)
                .into(mImageAuthor);

        userName.setText(mUser.getName());
        mTextSifnature.setText(TextUtils.isEmpty(mUser.getBio()) ? getResourceString(R.string.not_description) : mUser.getBio());
    }

    @Override
    protected void initData() {
        super.initData();

        mCollToolbar.setTitle(mUser.getUsername());
        mCollToolbar.setCollapsedTitleTextColor(Color.WHITE);
        mCollToolbar.setExpandedTitleColor(Color.TRANSPARENT);

        addFragment(R.id.fl_content, UserInfoViewPageFragment.newInstance(mProject));
    }
}
