package com.example.cc.mygit.view.userInfo.userEvents;

import android.content.Context;

import com.bumptech.glide.Glide;
import com.example.cc.mygit.R;
import com.example.cc.mygit.model.Event;
import com.example.cc.mygit.model.Project;
import com.haibin.androidbase.adapter.BaseRecyclerAdapter;
import com.haibin.androidbase.adapter.ViewHolder;

import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * Created by cc on 2017/3/18.
 */

public class UserEventsAdapter extends BaseRecyclerAdapter<Event> {

    public UserEventsAdapter(Context context, int mode) {
        super(context, ONLY_FOOTER);
    }

    @Override
    protected int getLayoutId(int viewType) {
        return R.layout.item_list_event;
    }

    @Override
    protected void onBindDefaultViewHolder(ViewHolder holder, Event item, int position) {
        String title = parseEventTitle(item.getAuthor().getName(), item.getProject().getOwner().getName()
                + " / " + item.getProject().getName(), item);
        holder.bindText(R.id.tv_name,title);

        Glide.with(mContext)
                .load(item.getAuthor().getNewPortrait())
                .asBitmap()
                .placeholder(R.mipmap.icon_git)
                .into(holder.getImageView(R.id.iv_portrait));
        //加载头像
    }

    private static final SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",
            Locale.US);

    protected String parseEventTitle(String author_name, String pAuthor_And_pName, Event event) {

        String title = "";
        String eventTitle = "";
        int action = event.getAction();
        switch (action) {
            case Event.EVENT_TYPE_CREATED:// 创建了issue
                eventTitle = event.getTarget_type() + getEventsTitle(event);
                title = "在项目 " + pAuthor_And_pName + " 创建了 " + eventTitle;
                break;
            case Event.EVENT_TYPE_UPDATED:// 更新项目
                title = "更新了项目 " + pAuthor_And_pName;
                break;
            case Event.EVENT_TYPE_CLOSED:// 关闭项目
                eventTitle = event.getTarget_type() + getEventsTitle(event);
                title = "关闭了项目 " + pAuthor_And_pName + " 的 " + eventTitle;
                break;
            case Event.EVENT_TYPE_REOPENED:// 重新打开了项目
                eventTitle = event.getTarget_type() + getEventsTitle(event);
                title = "重新打开了项目 " + pAuthor_And_pName + " 的 " + eventTitle;
                break;
            case Event.EVENT_TYPE_PUSHED:// push
                eventTitle = event.getData().getRef()
                        .substring(event.getData().getRef().lastIndexOf("/") + 1);
                title = "推送到了项目 " + pAuthor_And_pName + " 的 " + eventTitle + " 分支";
                break;
            case Event.EVENT_TYPE_COMMENTED:// 评论
                if (event.getEvents().getIssue() != null) {
                    eventTitle = "Issues";
                } else if (event.getEvents().getPull_request() != null) {
                    eventTitle = "PullRequest";
                }
                eventTitle = eventTitle + getEventsTitle(event);
                title = "评论了项目 " + pAuthor_And_pName + " 的 " + eventTitle;
                break;
            case Event.EVENT_TYPE_MERGED:// 合并
                eventTitle = event.getTarget_type() + getEventsTitle(event);
                title = "接受了项目 " + pAuthor_And_pName + " 的 " + eventTitle;
                break;
            case Event.EVENT_TYPE_JOINED:// # User joined project
                title = "加入了项目 " + pAuthor_And_pName;
                break;
            case Event.EVENT_TYPE_LEFT:// # User left project
                title = "离开了项目 " + pAuthor_And_pName;
                break;
            case Event.EVENT_TYPE_FORKED:// fork了项目
                title = "Fork了项目 " + pAuthor_And_pName;
                break;
            default:
                title = "更新了动态：";
                break;
        }
        title = author_name + " " + title;
        return title;
    }

    private static String getEventsTitle(Event event) {
        String title = "";
        if (event.getEvents().getIssue() != null) {
            title = " #" + event.getEvents().getIssue().getIid();
        }

        if (event.getEvents().getPull_request() != null) {
            title = " #" + event.getEvents().getPull_request().getIid();
        }
        return title;
    }
}


