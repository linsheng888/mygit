package com.example.cc.mygit.view.login;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.AppCompatEditText;
import android.view.View;

import com.example.cc.mygit.GitApplication;
import com.example.cc.mygit.R;
import com.example.cc.mygit.model.User;
import com.example.cc.mygit.view.MainActivity;
import com.haibin.androidbase.activity.BaseActivity;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by cc on 2016/11/12.
 */

public class LoginActivity extends BaseActivity implements LoginContract.View, View.OnClickListener{
    private LoginPresenter mPresenter;

    @Bind(R.id.et_email)
    AppCompatEditText mEditEmail;

    @Bind(R.id.et_pwd)
    AppCompatEditText mEditPassword;

    public static void show(Context context) {
        context.startActivity(new Intent(context, LoginActivity.class));
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_login;
    }

    @Override
    protected void initData() {
        super.initData();
        mPresenter = new LoginPresenter(this);
    }

    @OnClick(R.id.btn_login)
    @Override
    public void onClick(View v) {
        mPresenter.login(mEditEmail.getText().toString().trim(),
                mEditPassword.getText().toString().trim());

    }

    @Override
    public void showLoginSuccess(int strId, User user) {
        showToastShort(strId);
        GitApplication.getConfig().putUser(user);
        MainActivity.show(this);
        finish();
    }

    @Override
    public void showLoginError(int strId) {
        showToastShort(strId);

    }

    @Override
    public void showNetError(int strId) {
        showToastShort(strId);

    }

    @Override
    public void showEmailError(int strId) {
        showToastShort(strId);

    }

    @Override
    public void showPasswordEmptyError(int strId) {
        showToastShort(strId);

    }

    @Override
    public void setPresenter(LoginContract.Presenter presenter) {

    }
}
