package com.example.cc.mygit.view;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import com.example.cc.mygit.R;
import com.example.cc.mygit.view.language.project.LanguageViewPagerFragment;
import com.example.cc.mygit.view.message.MessageViewPagerFragment;
import com.example.cc.mygit.view.project.ProjectContract;
import com.example.cc.mygit.view.project.ProjectViewPagerFragment;
import com.example.cc.mygit.view.setting.SettingFragment;
import com.haibin.androidbase.activity.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;

public class MainActivity extends BaseActivity implements View.OnClickListener {

    @Bind({R.id.ib_find, R.id.ib_language, R.id.ib_message, R.id.ib_setting})
    ImageButton[] mTabButtons;

    private int mCurrentTab;
    private long mCurrentTime;
    private List<Fragment> mFragments = new ArrayList<>();

    public static void show(Context context) {
        context.startActivity(new Intent(context, MainActivity.class));
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected void initView() {
        super.initView();
        setSwipeBackEnable(false);//右滑不退出
        mFragments.add(ProjectViewPagerFragment.newInstance());
        mFragments.add(LanguageViewPagerFragment.newInstance());
        mFragments.add(MessageViewPagerFragment.newInstance());
        mFragments.add(SettingFragment.newInstance());

        addFragment(R.id.fl_content, mFragments.get(0));

    }

    @OnClick({R.id.ib_find, R.id.ib_language, R.id.ib_message, R.id.ib_setting})
    @Override
    public void onClick(View v) {
        int selectTab = 0;
        switch (v.getId()) {
            case R.id.ib_find:
                selectTab = 0;
                break;
            case R.id.ib_language:
                selectTab = 1;
                break;
            case R.id.ib_message:
                selectTab = 2;
                break;
            case R.id.ib_setting:
                selectTab = 3;
                break;
        }
        if (selectTab != mCurrentTab) {
            updateTab(selectTab);
            addFragment(R.id.fl_content, mFragments.get(selectTab));
            mCurrentTab = selectTab;
        }

    }

    private void updateTab(int selectItem) {
        mTabButtons[mCurrentTab].setColorFilter(getResources().getColor(R.color.gray));
        mTabButtons[selectItem].setColorFilter(getResources().getColor(R.color.colorAccent));
    }

    @Override
    public void onBackPressed() {

        if (System.currentTimeMillis() - mCurrentTime > 3000) {

            mCurrentTime = System.currentTimeMillis();
            showToastLong("再按一次推出APP");
        } else {
            super.onBackPressed();
        }
    }
}