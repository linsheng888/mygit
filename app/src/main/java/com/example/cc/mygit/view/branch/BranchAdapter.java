package com.example.cc.mygit.view.branch;

import android.content.Context;
import android.widget.CheckBox;

import com.example.cc.mygit.R;
import com.example.cc.mygit.model.Branch;
import com.haibin.androidbase.adapter.BaseRecyclerAdapter;
import com.haibin.androidbase.adapter.ViewHolder;

import static com.haibin.androidbase.adapter.BaseRecyclerAdapter.NEITHER;

/**
 * Created by cc on 2017/2/22.
 */

public class BranchAdapter extends BaseRecyclerAdapter<Branch> {
    public BranchAdapter(Context context) {
        super(context, NEITHER);
        mSelectedPosition = -1;
    }

    @Override
    protected int getLayoutId(int viewType) {
        return R.layout.item_list_branch;
    }

    @Override
    protected void onBindDefaultViewHolder(ViewHolder holder, Branch item, int position) {
        holder.bindText(R.id.tv_branch,item.getName());
        ((CheckBox)holder.getView(R.id.cb_branch)).setChecked(mSelectedPosition == position);
    }
}
