package com.example.cc.mygit.view.language;

import android.content.Context;
import android.support.v7.widget.SwitchCompat;

import com.example.cc.mygit.R;
import com.example.cc.mygit.model.Language;
import com.haibin.androidbase.adapter.BaseRecyclerAdapter;
import com.haibin.androidbase.adapter.ViewHolder;
import com.haibin.androidbase.utils.DateTimeFormat;

/**
 * Created by cc on 2016/12/7.
 */

public class LanguageAdapter extends BaseRecyclerAdapter<Language> {

    public LanguageAdapter(Context context) {
        super(context, ONLY_FOOTER);
    }

    @Override
    protected int getLayoutId(int viewType) {
        return R.layout.item_list_language;
    }

    @Override
    protected void onBindDefaultViewHolder(ViewHolder holder, Language item, int position) {

        holder.bindText(R.id.tv_name,item.getName());
        holder.bindText(R.id.tv_count,item.getProjectsCount()+"个项目");
        holder.bindText(R.id.tv_time,DateTimeFormat.format(item.getCreateDate()));
        SwitchCompat switchCompat = (SwitchCompat) holder.getView(R.id.switch_add);
        switchCompat.setChecked(item.isAdd());
    }
}
